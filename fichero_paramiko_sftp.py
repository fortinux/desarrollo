from getpass import getpass
import paramiko

host = 'IP_del_servidor'
user = 'usuario'


try:

    cliente = paramiko.SSHClient()
    # Se define utilizar la credencial del usuario
    # ATENCIÓN: AutoAddPolicy no protege contra ataques MITM
    # Más información en https://stackoverflow.com/q/10670217/850848#43093883
    cliente.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    password = getpass("Ingrese su clave ")
    cliente.connect(host, username=user, password=user)

    sftp_cliente = cliente.open_sftp()
    # # EJEMPLOS PARA SUBIR O DESCARGAR FICHEROS
    # # UTILIZANDO SFTP
    # # DESCOMENTAR UNO O AMBOS A LA VEZ
    # # Subir el fichero al directorio especificado
    # sftp_cliente.put(
    #     'texto.txt',
    #     '/home/usuario/texto.txt'
    # )
    # # Descargar un fichero
    # sftp_cliente.get(
    #     'texto_modificado.txt',
    #     '/home/usuario/texto_modificado.txt'
    # )

    # Crear una sesión remota utilizando un canal
    session = cliente.get_transport().open_session()
    if session.active:
        # Ejecutar un comando
        session.exec_command('cd /var/log && ls -la')
        # Obtener la salida del comando e imprimirla por pantalla
        resultado = session.recv(1024).decode()
        print(resultado)

    # Cerrar conexión
    sftp_cliente.close()
    cliente.close()

except paramiko.ssh_exception.AuthenticationException as e:
    print("Error de autenticación")
