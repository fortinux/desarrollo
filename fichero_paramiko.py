import time
from getpass import getpass
import paramiko

host = 'IP_del_servidor'
user = 'usuario'

try:

    cliente = paramiko.SSHClient()
    # Se define utilizar la credencial del usuario
    cliente.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    password = getpass("Ingrese su clave ")
    cliente.connect(host, username=user, password=user)

    # Se ejecuta el comando ls
    stdin, stdout, stderr = cliente.exec_command('ls')
    # Se espera un segundo para imprimir el resultado
    time.sleep(1)
    resultado = stdout.read().decode()
    print(resultado)
    # Cierra la conexión
    cliente.close()

# El bloque try-except devuelve este mensaje si hay un error de autenticación
except paramiko.ssh_exception.AuthenticationException as e:
    print("Error de autenticación")

