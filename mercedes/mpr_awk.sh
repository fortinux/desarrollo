#! /bin/bash
# Script: mpr_awk.sh
# Autor: mperezr
# Fecha: 21/01/2021
# Descripcion: Uso de awk para realizar ciertas acciones sobre un fichero de entrada
#
echo "                                          "
echo "  **  ------------------------------------------------------- **"
echo "  **  Uso de awk para mostrar datos de in fichero de entrada  **"
echo "  **  ------------------------------------------------------- **"
echo "                                          "
clear
read -p "Introduce fichero: " fichero
echo $fichero
#echo
#echo " Se muestra el nombre de usuario, ID de usuario e ID de grupo:"
#echo
#awk -F':' '{print $1,$3,$4}' $fichero
#
#Declaro la funcion continuar que solicita un enter para que mostrar por pantalla más resultados
continuar(){
    echo	 
    read -p " Pulsa Enter para continuar" fackEnterKey
}   
echo "Nombre de usuario"
echo "-----------------"
awk -F':' '{print $1}' $fichero
continuar
clear
echo "ID de usuario"
echo "-------------"
awk -F':' '{print $3}' $fichero
continuar
clear
echo "ID de grupo"
echo "-----------"
awk -F':' '{print $4}' $fichero
continuar
clear
numlineas=`awk 'END {print NR}' $fichero`
echo "Número de líneas del resultado" 
echo "------------------------------" 
echo $numlineas
echo
