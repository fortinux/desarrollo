#! /bin/bash
# Script: mpr_menu.sh
# Autor: mperezr
# Fecha: 17/01/2021
# Descripcion: Ofrece menu para consultar diversas situaciones del SO Linux
#
SALIR=0
while [ $SALIR -eq 0 ]; do
clear
echo "                                          "
echo "  ** -------------------------------------------------------- **"
echo "  **          Menú de consulta de actividad del Sistema       **"
echo "  ** -------------------------------------------------------- **"
echo "  **                                                          **"
echo "  **    1) Usuarios logueados                                 **"
echo "  **    2) Loguins no realizados                              **"
echo "  **    3) Configuración de red                               **"
echo "  **    4) Tabla de enrutamiento                              **"
echo "  **    5) Servicios de red activos                           **"
echo "  **    6) Mensajes del sistema                               **"
echo "  **    7) SALIR                                              **"
echo
#
# Se muestra un menu con 7 opciones
# el menu se vuelve a mostrar siempre que la opcion no sea 7) SALIR 
# para ello se inicializa el swich SALIR a 0
# se establece un bucle para mostrar el menu mientras SALIR=0
# y se pondra a 1 (SALIR=1) cuando se haya tecleado la opcion 7) SALIR
#
echo -n "        Teclea opción: "
read OPCION
case $OPCION in
	1)
	 	USUARIOSLOG=`who | cut -c 1-9`
		echo -e "\n Usuarios logueados: " 
                echo
	 	echo " $USUARIOSLOG"
                echo
		read -p " Pulsa Enter para otra consulta" fackEnterKey
;;  		
	2)
	 	USUARIOSNOLOG=`sudo lastb`
		echo -e "\n Loguins no realizados: "
                echo
		echo "$USUARIOSNOLOG"
                echo
		read -p " Pulsa Enter para otra consulta" fackEnterKey
;;  		
        3)
		echo -e "\n Configuración de red: "
                echo
		ifconfig
                echo
		read -p " Pulsa Enter para otra consulta" fackEnterKey
;;  		
        4)
		echo -e "\n Tabla de enrutamiento: "
                echo
		route
                echo
		read -p " Pulsa Enter para otra consulta" fackEnterKey
;;  		
        5)
		echo -e "\n Servicios de red activos: "
                echo
		nmcli device show|more
                echo
		read -p " Pulsa Enter para otra consulta" fackEnterKey
;;  		
        6)
		echo -e "\n Mensajes del sistema: "
                echo
		journalctl|tail -f
                echo
		read -p " Pulsa Enter para otra consulta" fackEnterKey
;;  		
        7)
		echo -e "\n Salgo del menú "
                SALIR=1
                echo
;;  		
*)
echo
echo "        Por favor elige una opción del 1 al 7"
echo
;;
esac
done  
exit
