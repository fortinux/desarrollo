#Nombre del proceso: num_par.py
#Autor: MPEREZR
#Fecha: 25/03/2021
#Descripción: Pide número al usuario y dice si es par o impar. En caso de ser múltiplo de 4 lo comunica

### Introducir un número por teclado y decir si es par o impar
print("\nDetectar si número par o impar")
print("==============================\n")
#
num=int(input('Introduzca un numero: '))
if(num % 2 == 0):
 print('Número Par')
 if(num % 4 == 0):
  print('Número múltiplo de 4')
else:
 print('Número Impar')
