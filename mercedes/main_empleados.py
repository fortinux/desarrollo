from clase_empleados import empleado

empleado1 = empleado("Mercedes", "Perez", 224466)
empleado1.aptitudes.lenguajes = "Cobol";
empleado1.aptitudes.so = "AS-400";
empleado1.aptitudes.idioma = "Ingles";

#def add_idioma(self, nuevo_idioma):
#   self.idioma.append(nuevo_idioma);


empleado1.__str__();
#print (" \nCodifico la impresión desde el main\n")
print("\n")
print("    NOMBRE: ", empleado1.nombre);
print("    APELLIDOS: ", empleado1.apellido);
print("    NUMERO DE EMPLEADO: ", empleado1.num_empleado);
print("    LENGUAJES: ", empleado1.aptitudes.lenguajes);
print("    SISTEMAS OPERATIVOS: ", empleado1.aptitudes.so);
print("    IDIOMA: ", empleado1.aptitudes.idioma);
print("\n")


empleado2 = empleado("Bill", "Gates", 111111)
empleado2.aptitudes.lenguajes = "Basic";
empleado2.aptitudes.so = "Windows";
empleado2.aptitudes.idioma = "Ingles";
#empleado2.__str__()
print("    NOMBRE: ", empleado2.nombre);
print("    APELLIDOS: ", empleado2.apellido);
print("    NUMERO DE EMPLEADO: ", empleado2.num_empleado);
print("    LENGUAJES: ", empleado2.aptitudes.lenguajes);
print("    SISTEMAS OPERATIVOS: ", empleado2.aptitudes.so);
print("    IDIOMA: ", empleado2.aptitudes.idioma);

