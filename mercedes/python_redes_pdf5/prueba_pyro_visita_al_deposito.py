import Pyro4
from prueba_pyro_clientes import Person

# URI:PYRO:example.warehouse@localhost:37049
uri = input("Enter the uri of the warehouse: ").strip() #

warehouse = Pyro4.Proxy(uri)
janet = Person("Janet")
henry = Person("Henry")
janet.visit(warehouse)
henry.visit(warehouse)