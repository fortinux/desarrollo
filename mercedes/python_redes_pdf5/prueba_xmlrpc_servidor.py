from xmlrpc.server import SimpleXMLRPCServer
import logging
import os
# Configura el nivel del log
logging.basicConfig(level=logging.INFO)

# Configura el servidor en el puerto 10000
server = SimpleXMLRPCServer(('localhost', 10000), logRequests=True)

# Crea el servicio: una función que lista el contenido de un directorio
def lista_contenidos(dir_name):
 logging.info('lista_contenidos(%s)', dir_name)
 return os.listdir(dir_name)

server.register_function(lista_contenidos)

# Inicia el servidor
try:
 print('Use Control-C to exit')
 server.serve_forever()
except KeyboardInterrupt:
 print('Exiting')
