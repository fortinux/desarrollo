import os
import sqlite3
db_filename = '../../todo.db'
schema_filename = 'todo_schema_2.sql'  # la encuentro en -> https://rico-schmidt.name/pymotw-3/sqlite3/
db_is_new = not os.path.exists(db_filename)
with sqlite3.connect(db_filename) as conn:
    if db_is_new:
         print('Creando schema')
         with open(schema_filename, 'rt') as f:
            schema = f.read()
         conn.executescript(schema)
         print('Adjuntando datos iniciales')

         conn.executescript("""
         insert into project (name, description, year)
         values ('Python', 'Python Rocks!',
         '1991');

         insert into task (details, status, year, project)
         values ('Config application', 'done', '2014',
         'Plone');

         insert into task (details, status, year, project) values ('Create module', 'done', '2014',
         'Plone');

         insert into task (details, status, year, project)
         values ('Python course', 'active', '2021',
         'Desarrollo');
         """)
    else:
        print('La base de datos existe, por ende ya tiene su schema.')