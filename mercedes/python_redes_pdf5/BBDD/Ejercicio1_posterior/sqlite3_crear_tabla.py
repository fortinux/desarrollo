import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    """ Crea una conexión a SQLite. Ejemplo adaptado de:
         https://www.sqlitetutorial.net/sqlite-python/create-tables/
         :param db_file: base de datos
         :return: Objeto conexión o None
         """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
         print(e)
    return conn

def create_table(conn, create_table_sql):
    """ Crea una tabla usando create_table_sql
        :param conn: Objeto conexión
    """
    try:
         c = conn.cursor()
         c.execute(create_table_sql)
    except Error as e:
        print(e)

def main():
    database = 'sqlite_proyectos.db' # Ejemplo ruta en Windows: r"C:\sqlite\db\sqlite_proyectos.db"

    sql_create_projects_table = """CREATE TABLE IF NOT EXISTS proyectos (
         id integer PRIMARY KEY,
         cliente text NOT NULL,
         fecha_inicio text,
         fecha_fin text
         ); """
    sql_create_tasks_table = """CREATE TABLE IF NOT EXISTS tareas (
         id integer PRIMARY KEY,
         nombre text NOT NULL,
         prioridad integer,
         status_id integer NOT NULL,
         id_proyecto integer NOT NULL,
         fecha_inicio text NOT NULL,
         fecha_fin text NOT NULL,
         FOREIGN KEY (id_proyecto) REFERENCES proyectos (id)
         );"""
    # Crea la conexión a la base de datos
    conn = create_connection(database)
    # Crea las tablas
    if conn is not None:
         # Crea tabla proyectos
         create_table(conn, sql_create_projects_table)
         # Crea     tabla tareas
         create_table(conn, sql_create_tasks_table)
         conn.commit()
    else:
        print("Error! No se puede crear la conexión a la bas de datos.")
#
if __name__ == '__main__':
    main()

