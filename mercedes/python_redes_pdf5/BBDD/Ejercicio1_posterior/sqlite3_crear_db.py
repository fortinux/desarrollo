import sqlite3
from sqlite3 import Error
def create_connection(db_file):
     # Crea una conexión a SQLite
     # Si la base de datos no existe la crea
     conn = None
     try:
             conn = sqlite3.connect(db_file)
             print(sqlite3.version)
     except Error as e:
            print(e)
     finally:
         if conn:
            conn.close()
if __name__ == '__main__':
 create_connection('sqlite_proyectos.db')