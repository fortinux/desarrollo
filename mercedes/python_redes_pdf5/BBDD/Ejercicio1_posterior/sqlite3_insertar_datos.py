import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn

def create_project(conn, proyectos):
    sql = """INSERT INTO proyectos(cliente,fecha_inicio,fecha_fin) VALUES(?,?,?)"""
    cur = conn.cursor()
    cur.execute(sql, proyectos)
    conn.commit()
    return cur.lastrowid
def create_task(conn, tareas):
    sql = """INSERT INTO tareas(nombre,prioridad,status_id,id_proyecto,fecha_inicio,fecha_fin) VALUES(?,?,?,?,?,?) """
    cur = conn.cursor()
    cur.execute(sql, tareas)
    conn.commit()
    return cur.lastrowid

def main():
    database = "sqlite_proyectos.db"
    conn = create_connection(database)
    with conn:
        # Crea un nuevo proyecto
        proyectos_1 = ('App hecha con SQLite & Python', '2021-01-01', '2021-05-30');
        id_proyecto = create_project(conn, proyectos_1)

        # Tareas
        tareas_1 = ('Primer sprint', 1, 1, id_proyecto, '2021-01-01', '2021-02-01')
        tareas_2 = ('Segundo sprint', 1, 1, id_proyecto, '2021-02-01', '2021-03-01')

        # Crea las tareas
        create_task(conn, tareas_1)
        create_task(conn, tareas_2)

if __name__ == '__main__':
    main()