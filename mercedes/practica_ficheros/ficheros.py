print("====================================");
print(">> Práctica de manejo de Ficheros <<");
print("====================================");

##################################################################################################
print("1.- Crear una copia y luego convertir el fichero .csv a .tsv");
import shutil
import pandas as pd
origen = "01001c.csv";
destino = "01001c.tsv";
# Hacemos una copia del fichero
#shutil.copy(src=origen, dst="copia_"+origen);

# Convertimos el fichero a .tsv
# shutil.copy(src=origen, dst=destino);
#
# print("    - Fichero origen    =>",origen );
# print("    - Fichero backup    => copia_"+origen );
# print("    - Copiado a destino =>",destino);
leer_csv = pd.read_csv("fichero.csv", sep=',')
with open("escribir_tsv.tsv",'w', encoding='utf-8', newline='') as write_tsv:
    write_tsv.write(leer_csv.to_csv(sep='\t', index=False))
write_tsv.close()
##################################################################################################
print("2.- Crear un directorio y mover la copia del fichero .csv dentro");
from pathlib import Path
import os

#fichero_path = Path(Path.home(), "directorio_ficheros");   # Directorio Home del usuario
fichero_path = Path("./", "directorio_ficheros");           # Directorio donde se esta ejecutando

# Si no existe el directorio, se agrega
# if not fichero_path.exists():
#     os.makedirs(fichero_path);
#     print("    - Creamos el directorio =>",fichero_path);
# else:
#     print("    - El directorio ",fichero_path , " ya existe");

# Mover al igual que el comando mv en GNU/Linux
# shutil.move(src="./copia_01001c.csv", dst=fichero_path)
# print("    - Fichero copia_01001c.csv movido a ", fichero_path);

##################################################################################################
print("3.- Escribir en un nuevo fichero las primeras 100 líneas del fichero .tsv");

# Leemos el fichero 01001c.tsv
# with open("./01001c.tsv", 'r') as file:
#     contenido = file.readlines()
#     print("    - Leemos todo el fichero => 01001c.tsv");

# Creamos el nuevo fichero y escribimos en el las 100 primeas lineas
# nuevo_fichero="./nuevo_fichero.txt"
# with open(nuevo_fichero, 'w') as file:  #Con w indicamos que file va a ser para escritura
#     i = 1;
#     for linea in contenido:
#         if i <= 100:
#             file.write(linea)
#             i = i+1;
#         else:
#             break;
#     print("    - Creamos el fichero => nuevo_fichero.txt y en él escribimos las 100 primeras lineas del fichero 01001c.tsv");

#################################################################################################
print("4.- Comparar los datos dentro de los ficheros .csv");

import filecmp
# Directorios a comparar
dir1 = "./"
dir2 = "./directorio_ficheros"

# Ficheros comunes dentro de los directorios a comparar
comunes = ["01001c.csv", "copia_01001c.csv", "01001.xlsx"]

print("    - Comparamos los ficheros", comunes, "de los directorios", dir1, "y", dir2);

# Comparar los metadatos de los ficheros comunes
match, mismatch, errors = filecmp.cmpfiles(dir1, dir2, comunes)
print("      * Verifica los metadatos:")
print("             Coinciden:", match)
print("             NO coinciden:", mismatch)
print("             Errores:", errors, "\n")

# Compara también los datos dentro de los ficheros
match, mismatch, errors = filecmp.cmpfiles(dir1, dir2, comunes, shallow=False)
print("      * Verifica los Datos:")
print("             Coinciden:", match)
print("             NO coinciden:", mismatch)
print("             Errores :", errors)

##################################################################################################
print("5.- Exportar las primeras 5 columnas del fichero en formato .xlsx a un nuevo fichero.");
import pandas as pd
#pip install pandas desde terminal
#pip install xlrd desde terminal
#pip install openpyxl desde terminal

# Si no queremos que salga el warning por pantalla, lo metemos dentro de este with
#import warnings
#with warnings.catch_warnings(record=True) as w:

# Se crea el dataframe con pandas de un fichero Excel
dataframe_excel = pd.read_excel("./01001.xlsx")
print("    - Leemos el fichero Excel  => 01001.xlsx ...");

# Se exporta a excel las 5 primeras columnas
dataframe_excel.iloc[:, [0, 1, 2, 3, 4]].to_excel('01001_5_columnas.xlsx', index=False)
print("    - ... y copiamos las 5 primeras columnas en el fichero => 01001_5_columas.xlsx");

print("======================");
print(">> FIN DEL PROGRAMA <<");
print("======================");
