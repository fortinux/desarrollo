#! /bin/bash
# Script: mpr_parametros.sh
# Autor: mperezr
# Fecha: 11/01/2021
# Descripcion: Muestra parametros especiales de Bash
#
echo "                                          "
echo "  **  --------------------------------- **"
echo "  **  Práctica de Parámetros Especiales **"
echo "  **  --------------------------------- **"
echo "                                          "
# Mostrar el nombre del script
echo "El nombre de este script es $0"
echo "  "
# Mostrar el PID del proceso
echo "El PID del proceso actual es $$"
echo "  "
# Ejecución de un comando en segundo plano
sleep 100 &
# Mostrar el PID del proceso ejecutado en un segundo plano
echo "El PID del último comando ejecutado en segundo plano es $!"
echo "  "
# Mostrar el primer parámetro
echo "El primer parámetro es $1"
echo "  "
# Mostrar el segundo parámetro
echo "El segundo parámetro es $2"
echo "  "
# Mostrar el tercer parámetro
echo "El tercer parámetro es $3"
echo "  " 
# Mostrar el número de parámetros
echo "El numero total de parametros es $#"
echo "  "
# Lista de parámetros (un solo argumento)
echo "Todos estos parametros son $*"
#
for param in "$*"; do 
    echo " Lista de parametros: $param" 
done
echo "  "
# Lista de parámetros (un parámetro por argumento)
echo 'Se puede ver cada elemento de la lista buscando en $@ :' $@
for param in "$@"; do 
    echo -e "\tParámetro: $param "
done
echo "  "
echo "El código de retorno del último comando ha sido $?"
echo "  "
