#! /bin/bash
# Script: mpr_variables.sh
# Autor: mperezr
# Fecha: 11/01/2021
# Descripcion: Solicita Nombre y edad del usuario y le da la bienvenida a SO Linux
#
echo "                                          "
echo "  **  ------------------------------------------------------- **"
echo "  **  Práctica de Variables solictadas por línea de comandos  **"
echo "  **  ------------------------------------------------------- **"
echo "                                          "
echo -n "¿Cual es su nombre?: "
read nombre apellido1 apellido2
echo "  "
echo -n  "¿Cual es su edad?: "
read edad 
echo "  "
echo "Hola $nombre $apellido1 $apellido2, bienvenido al SO Linux"
echo "  "
exit
