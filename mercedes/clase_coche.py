# Ejemplo clase
# Creando la clase
class Coche:
    def __init__(self, marca, modelo, tipo):
        self.marca = marca;
        self.modelo = modelo
        self.tipo = tipo
        self.capacidad_gasolina = 15
        self.nivel_gasolina = 0

    def gasolina_completo(self):
        self.nivel_gasolina = self.capacidad_gasolina
        print('El depósito de gasolina está lleno')
    def conducir(self):
        print(f'El {self.modelo} se está conduciendo.')
