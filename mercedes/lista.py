#Nombre del proceso: lista.py
#Autor: MPEREZR
#Fecha: 25/03/2021
#Descripción: Se toma una lista de entre 5 y 10 números y se hace otra con el primer y último elemento de la misma.

print("\nPrimer y último número de una lista aleaoria de entre 5 y 10 números")
print("====================================================================")
#
#Se importa la función de enteros aleatorios
from random import randint

# se declara las listas vacías
lista1=[]
lista2=[]
#De entre un rango de enteros entre 5 y 10 números, añadimos a la lista números aleatorios entre el -10 y el 10.
for i in range (randint (5,10)):
    lista1.append (randint(-10,10))
#
#muestro la lista aleatoria entre 5 y 10 números
print("Lista aleatoria: ", (lista1))

#El primer número de la lista está en la posición 0, en nuestro caso lista1[0]
print ("El primer número de la lista es: ", lista1[0])

#El último número de la lista está en la posición -1, en nuestro caso lista1[-1]
print ("El último número de la lista es: ", lista1[-1])

#El último número de la lista también se puede coger en la posición
# longitud de de la lista menos 1, lista1[len(lista1)-1]
print ("Repito, el último número de la lista es: ", lista1[len(lista1)-1])
lista2.append(lista1[0])
lista2.append(lista1[-1])
print("La nueva lista es: ", (lista2))