##################################################################################################
print(' 1- Ejemplo crear fichero JSON');
##################################################################################################
import json
# Ejemplo de string en JSON
persona = '{"nombre": "Fortinux", "lenguajes": ["Python", "PHP"]}'
print("     Json Origen:",persona)

# Parse usando el método json.loads()
# Crea un diccionario en python
persona_dic = json.loads(persona)
print("     Lenguajes dentro del Json", persona_dic['lenguajes'])

# Convierte diccionario a JSON
# .dumps (encode) toma un diccionario como input y devuelve un string
persona_json = json.dumps(persona_dic)
print("     dumps de persona (vemos que es el json original):", persona_json)
##################################################################################################
print(" 1- Ejemplo manipular fichero JSON");
##################################################################################################
# Leer un fichero JSON
with open('./peliculas.json') as fichero:
    print("     Leemos el fichero : ./peliculas.json")
    # .loads (decode) toma un string como input y devuelve un diccionario
    datos = json.loads(fichero.read())
print("     El contenido del fichero es:",datos);

print("     que coincide con el contenido de datos[peliculas]:", datos["peliculas"]);
exit()
# pero para ver el contenido de nombre o director no se puede hacer  datos["nombre"] o datos[director]
# hay que hacerlo de la siguiente forma
for pelicula in datos['peliculas']:
    print('Nombre pelicula:', pelicula['nombre'])
    print('Nombre director:', pelicula['director'],'\n')

# Exporta “datos” a un fichero .json
with open('peliculas_clasificacion.json', 'w') as json_fichero:
    json.dump(datos["clasificaciones"], json_fichero)

