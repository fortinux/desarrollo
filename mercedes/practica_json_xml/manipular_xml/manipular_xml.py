##################################################################################################
print("4. Ejemplo manipular XML");
##################################################################################################
import xml.etree.ElementTree as ET
# Importar XML desde un fichero
tree = ET.parse('peliculas.xml')
root = tree.getroot()
print("     El elemento raiz del xml 'pelicuals.xml' es:",root);

# Acceder a elementos determinados
print("\n     Accedemos a los Hijos de priner nivel y sus atributos de root", root)
for peli in root:
    print("     ->", peli.tag, peli.attrib)
#
# Acceder al child utilizando índices
print("\n     Accedemos ditectamente a uno de los elementos del XML a traves de indices (recordamos que los indices empiezan en 0)")
print("     -> El elemento root[0][1].tag es :", root[0][1].tag);
print("     -> El elemento root[0][1].text es :", root[0][1].text);
print("     -> El elemento root[0][1].attrib es :", root[0][1].attrib);

# Acceder a sub elementos utilizando iteraciones en elementos de root
print("\n     Accedemos los elementos del XML a traves de iteraciones de root")
for pp  in root.iter('Actor'):
    print("     ->",pp.text, pp.attrib)
#
# Modificar un fichero XML y escribirlo a un nuevo fichero
# Modificamos el xml mofificando el valor del tag Formato de DVD a 4k o atributo "actualizado"
for formato in root.iter('Formato'):
    formato.text = "4K"
    formato.set('actualizado', 'si')
tree.write('peliculas_modificado.xml')

