######  CREAR HASH  ########

print(" 1. Ejemplo crear hash. Pruebo ejemplo del pdf ");
##################################################################################################
# Ver hash en python en https://pythondiario.com/2017/09/algoritmos-hash-criptografia-con-python.html#:~:text=Los%20hash%20o%20funciones%20hash,que%20se%20le%20ha%20dado.
import hashlib
# Se crea una variable con el fichero a ser usado para el hash
nombre = './prueba.txt'
print("     Nombre del fichero", nombre);

# Se convierte el formato a bytes usando 'encode'
# Las funciones hash solamente aceptan esta codificación
nombre_codificado = nombre.encode()
print("     Nombre codificado del fichero", nombre_codificado);

nombre_hash = hashlib.sha256(nombre_codificado)

# Ya creado el hash, se imprime la versión hexadecimal del mismo usando el método 'hexdigest()'
print("     Object:", nombre_hash)
print("     Hexadecimal format:", nombre_hash.hexdigest())

##################################################################################################
print("\n 2. Cifrar con sha-256 y md5 ");
m= hashlib.sha256(b"mensaje")
print("\n   Resultado con algoritmo sha-256 y metodo de salida de datos digest: \n", m.digest())
m= hashlib.sha256(b"Mensaje")
print("\n   Resultado con solo poner la m en mayúscula: \n", m.digest())
m= hashlib.md5(b"mensaje")
print("\n   Resultado con algoritmo md5 y metodo de salida de datos digest: \n", m.digest())
print("\n   Resultado con algoritmo md5 y metodo de salida de datos hexdigest: \n", m.hexdigest())
m= hashlib.sha256(b"mensaje")
print("\n   Tamaño del hast en bytes usando el atributo digest_size: ", m.digest_size)