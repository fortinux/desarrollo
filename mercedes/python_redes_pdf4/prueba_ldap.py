# Ejemplo de conexión anónima y sincrónica mediante LDAP
# Adaptado de https://ldap3.readthedocs.io/en/latest/tutorial_intro.html
from ldap3 import Server, Connection, ALL
# Se conecta al servidor LDAP
server = Server('ipa.demo1.freeipa.org', get_info=ALL)
# Autentica de forma anónima y sincrónica
conn = Connection(server, auto_bind=True)
print(conn.extend.standard.who_am_i())
# Obtiene información sobre el server y el schema utilizado
#print(server.info)
#print(server.schema)
#print(conn)

from ldap3 import Server, Connection, ALL, NTLM
server = Server('ipa.demo1.freeipa.org', get_info=ALL)
conn = Connection(server, 'uid=admin,cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org','Secret123', auto_bind=True)
print(conn.extend.standard.who_am_i())
print(conn)
#
from ldap3 import Server, Connection, Tls
import ssl
# Este servidor de ejemplo no puede verificar el certificado
# El Tls debería ser: Tls(validate=ssl.CERT_REQUIRED
tls_configuration = Tls(validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1)
server = Server('ipa.demo1.freeipa.org', use_ssl=True, tls=tls_configuration)
conn = Connection(server)
print(conn)







