from aptitudes import aptitudes

class empleado(aptitudes):
    def __init__(self, nombre, apellido, num_empleado):
        self.nombre = nombre;
        self.apellido = apellido;
        self.num_empleado = num_empleado;
        self.aptitudes = aptitudes;

    def __str__(self):
        print("    NOMBRE: ",  self.nombre);
        print("    APELLIDOS: ", self.apellido);
        print("    NUM.EMPLEADO: ", self.num_empleado);
        print(self.aptitudes.__str__(self));


    def print_nombre_completo(self):
        print(self.nombre, self.apellido);

    def print_atitudes(self):
        print(self.aptitudes.__str__());



