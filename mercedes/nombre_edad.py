#Nombre del proceso: nombre_edad.py
#Autor: MPEREZR
#Fecha: 25/03/2021
#Descripción: Pide nombre y edad al usuario y muestra año en que cumplirá 100 años

#Se importan las dependencias necesarias
from datetime import date
from datetime import datetime

#Día actual
today = date.today()
anyoActual = today.year

print("\nAño en el que cumplirás 100 años")
print("================================\n")

nombre = input("Introduce tu nombre: ")
edad = int(input("Introduce tu edad: "))
diferenciaHasta100 = 100 - edad
print("Para cumplir 100 años te quedan: ", diferenciaHasta100)
print("Año actual:", anyoActual)
cumplire100anyos = anyoActual + diferenciaHasta100
print(nombre,", cumplirás 100 años en el año: ", cumplire100anyos)