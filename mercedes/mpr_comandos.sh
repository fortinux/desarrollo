#! /bin/bash
# Script: mpr_comandos.sh
# Autor: mperezr
# Fecha: 11/01/2021
# Descripcion: Muestra los ficheros de directorio actual y los de otro directorio pasado por parámetro
#
echo "                                          "
echo "  **  ---------------------------------------------------------------- **"
echo "  **  Lista Ficheros del Directorio actual y del pasado por parámetro  **"
echo "  **  ---------------------------------------------------------------- **"
echo "                                          "
DIR_ACTUAL=`pwd`
LISTA_FICHEROS=`ls -ls`
echo "  "
echo "Estoy en el directorio $DIR_ACTUAL que contiene estos ficheros:"
echo "  "
echo "$LISTA_FICHEROS "
echo "  "
# Nos situamos en el directorio que indica el parámetro pasado
cd $1
LISTA_FICHEROS=`ls -ls`
echo "Paso por parámetro el directorio $1 que contiene estos ficheros:"
echo "  "
echo "$LISTA_FICHEROS "
exit
