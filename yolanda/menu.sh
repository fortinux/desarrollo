#!/bin/bash
#creo un bucle infinito para seguir mostrando el menu hasta elegir la opcion 7
while :
do
	echo -e "\e[0m"
	echo "********* MENU *********************"
	echo "1) Usuarios logueados"
	echo "2) Logins no realizados"
	echo "3) Configuracion de red"
	echo "4) Tabla de enrutamiento"
	echo "5) Servicios de red activos"
	echo "6) Mensajes del sistema"
	echo "7) SALIR"
	read -p "Seleccione la opcion (1 - 7):" opcion
	echo "*************************************"
	case $opcion in
	1)
	#escribe la salida en color azul (34)"
		echo -e "\e[34m Usuarios logados" #azul
		who;;#actuales / last logados alguna vez
	2)
		echo -e "\e[31m Logins no realizados" #rojo
		sudo lastb;;
	3)
		echo -e "\e[93m Configuracion de red" #amarillo
		ip a;;
	4)
		echo -e "\e[32m Tabla de enrutamiento" #verde
		route;; #tambien ip route
	5)
		echo -e "\e[95m Servicios de red activos" #magenta
		netstat -atup;; #sockets, tcp, udp
	6)
		echo -e "\e[96m Mensajes del sistema\e[0m" #cyan
		dmesg -HT -l err,emerg,crit,alert ;; #errores (warn, notice, info, debug
	7)
		echo -e "\e[97;42m Hasta pronto :)\e[0m" #blanco sobre fondo verde
		exit;;
	*)
		echo -e "\e[97;104m Opcion incorrecta\e[0m";;#blanco sobre fondo azul

	esac
done



