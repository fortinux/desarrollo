from colorama import init, Fore, Back, Style
init()


class Coche:
    # Creando la clase
    def __init__(self, marca, modelo, color):
        self.marca = marca
        self.modelo = modelo
        self.color = color
        self.capacidad_deposito = 15
        self.nivel_combustible = 2

    def consulta_deposito(self):
        if self.nivel_combustible == self.capacidad_deposito:
            print(Fore.LIGHTMAGENTA_EX+f'El depósito está lleno'+'\033[39m')
        else:
            # cálculo del porcentaje de combustible restante
            queda = 100 * float(self.nivel_combustible) / float(self.capacidad_deposito)
            if queda < 20:
                # azul
                print(Back.YELLOW+Fore.LIGHTCYAN_EX +
                      f'Tiene que repostar!!!.'
                      f'Queda {queda:.3f} % de combustible en el depósito'+Style.RESET_ALL)
            else:
                # amarillo
                print('\033[33m'+f"Suficiente combustible. Le queda: {queda:.3f}%"+'\033[39m')

    def llenar_deposito(self):
        self.nivel_combustible = self.capacidad_deposito
        # cyan
        print('\033[36m'+f"Fin de repostaje --> Nivel combustible: {self.nivel_combustible}"+'\033[39m')

    def llenar_medio_deposito(self):
        self.nivel_combustible = self.capacidad_deposito/2
        # verde
        print('\033[32m'+f"Medio depósito -->Nivel combustible: {self.nivel_combustible}"+'\033[39m')

    def vaciar_deposito(self):
        self.nivel_combustible = 0
        # rojo
        print('\033[31m'+f"Depósito vacío -->Nivel combustible: {self.nivel_combustible}"+'\033[39m')

    def menu(self):
        opcion = 0
        while opcion != 5:
            print("1- Consultar depósito")
            print("2- Llenar depósito completo")
            print("3- Vaciar depósito")
            print("4- Llenar medio depósito")
            print("5- Finalizar")
            opcion = int(input("Elija su opcion:"))
            if opcion == 1:
                self.consulta_deposito()
            elif opcion == 2:
                self.llenar_deposito()
            elif opcion == 3:
                self.vaciar_deposito()
            elif opcion == 4:
                self.llenar_medio_deposito()
