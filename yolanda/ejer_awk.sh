#!/bin/bash
#se comprueba que tenga un parametro de entrada
if [ $# -gt 0 ]
then
  #se comprueba que el fichero de entrada sea el correcto /etc/passwd	
  if [ "$1" == "/etc/passwd" ]
  then
    #se escribe el nombre del fichero de entrada en amarillo	  
    echo -e "\e[93m El fichero de entrada es:" "$1" "\e[0m"
    #se escribe la cabecera del awk en azul
    echo -e "\e[34m"
    #el awk obtiene las columnas usuario, uid y gid y el recuento del numero de lineas totales
    awk 'BEGIN {printf "%-22s %6s %6s", "Usuario","UID","GID\n"}{printf "\033[0m"}{printf "%-22s %6d %6d\n", $1, $3, $4}
   END {printf "\033[1;31m El numero de lineas del fichero es:" NR "\033[0m\n"}' FS=":" "$1" 
  else
    echo -e "\e[44m awk no preparado para el fichero" "$1" "\e[0m" #fondo azul
  fi
else
  echo -e "\e[41m Error: sin parametro de entrada\e[0m"  #fondo rojo
fi
