import requests  # para obtener el fichero de la url
import pandas as pd  # para leer csv (read_csv)y xlsx (read_xlsx)
import os  # para rutas
import shutil  # para copia del fichero csv
import csv  # para convertir el fichero csv a tsv csv.reader, csv.writer
import filecmp  # para comparar los ficheros csv


# metodo para descargar el fichero csv de la url a un fichero local
def descarga_csv_a_local(url, fich_local):
    resp = requests.get(url)
    # print(resp.encoding)
    with open(fich_local, 'wb') as output:
        output.write(resp.content)
    # importante indicar el separador para que no de Error tokenizing data
    df = pd.read_csv(fich_local, sep=';')
    # return df


# metodo para descargar el fichero xlsx de la url a un fichero local
def descarga_xlsx_a_local(url_xlsx, fich_xlsx_local):
    resp = requests.get(url_xlsx)
    with open(fich_xlsx_local, 'wb') as output:
        output.write(resp.content)
    df2 = pd.read_excel(fich_xlsx_local)
    # return df


# --------------------------------------------------------------------------------
# Paso 0 descarga de url a fichero local
# --------------------------------------------------------------------------------
# definicion de variables
# encoding="utf-8 en el fichero abajo dcha"
url_descarga = 'https://datos.madrid.es/egob/catalogo/300110-22-accidentes-bicicleta.csv'  # url descarga
f_csv_local = 'BICI2_CSV_LOCAL.csv'  # nombre fichero en local
ruta = os.getcwd() + os.sep  # directorio local
descarga_local = ruta + f_csv_local

# llamo al metodo para descargar el fichero csv de la url a un directorio local
descarga_csv_a_local(url_descarga, descarga_local)
print(f" PASO 0 --> Realizada la descarga del csv de la URL al fichero LOCAL {descarga_local}")
# --------------------------------------------------------------------------------
# paso 1A CREAR UNA COPIA DEL FICHERO CSV LOCAL
# --------------------------------------------------------------------------------
# definicion de variables
f_copia_csv = 'BICI2_copia_CSV_LOCAL.csv'
destino_copia = ruta + f_copia_csv

if os.path.exists(descarga_local):
    with open(descarga_local, 'rb') as forigen:
        with open(destino_copia, 'wb') as fdestino:
            shutil.copyfileobj(forigen, fdestino)
            print(f" PASO 1A --> Archivo {f_csv_local} COPIADO en {f_copia_csv} en el directorio {ruta}")


# --------------------------------------------------------------------------------
# PASO 1B CONVERTIR EL FICHERO CSV LOCAL A TSV
# --------------------------------------------------------------------------------
f_dest_tsv = 'BICI2_tsv_LOCAL.tsv'
destino_tsv = ruta + f_dest_tsv

with open(descarga_local, 'r', encoding='utf-8') as csvin, \
        open(destino_tsv, 'w', newline='', encoding='utf-8') as tsvout:
    csvin = csv.reader(csvin, delimiter=';')
    tsvout = csv.writer(tsvout, delimiter='\t')

    for row in csvin:
        tsvout.writerow(row)

print(f" PASO 1B --> Fichero csv {f_csv_local} convertido a tsv {f_dest_tsv}")

# -----------------------------------------------------------------------------------------
# PASOS 2A Y 2B CREAR UN DIRECTORIO Y MOVER EL FICHERO COPIA CSV A EL
# -----------------------------------------------------------------------------------------
nuevo_dir = 'BICI'
nueva_ruta = ruta + nuevo_dir + os.sep
nuevo_destino_copia = nueva_ruta + f_copia_csv

if not os.path.exists(nueva_ruta):
    os.mkdir(nueva_ruta)  # si no existe el directorio lo creo
    print(f" PASO 2A --> Creado directorio {nueva_ruta}")
else:
    print(f' PASO 2A --> Ya existe el directorio {nueva_ruta}')

if os.path.isfile(destino_copia):
    # si existe el fichero que quiero mover en el directorio origen
    if not os.path.isfile(nuevo_destino_copia):
        # si no existe el fichero en el directorio destino, moverlo
        shutil.move(destino_copia, nueva_ruta)
        print(f" PASO 2B --> Movido el fichero {destino_copia} al directorio {nueva_ruta}")
    else:
        print(f" PASO 2B --> Ya existe el fichero {f_copia_csv} en el directorio {nueva_ruta} ")
else:
    print(f" PASO 2B --> No existe el fichero {destino_copia} para moverlo a {nueva_ruta}")

# -----------------------------------------------------------------------------------------
# PASO 3 --> Escribir las 100 primeras lineas del fichero TSV en un fichero nuevo
# -----------------------------------------------------------------------------------------
# destino_tsv es el fichero tsv que convertimos del csv
f_nuevo_tsv = 'BICI2_100_tsv_LOCAL.tsv'
nuevo_tsv = ruta + f_nuevo_tsv

if os.path.isfile(destino_tsv):
    # si existe el fichero del que quiero obtener las primeras 100 lineas
    df2_local = pd.read_csv(destino_tsv, encoding='utf-8', delimiter='\t')
    df2_local.iloc[0:100].to_csv(nuevo_tsv, encoding='utf-8', sep='\t')  # index=False
    print(f" PASO 3 --> Creado el fichero {nuevo_tsv} con 100 lineas del fichero {destino_tsv}")
else:
    print(f" PASO 3 --> No existe el fichero {destino_tsv} , origen para crear el nuevo {nuevo_tsv}")


# -----------------------------------------------------------------------------------------
# Comparar los datos dentro de los ficheros .csv
# -----------------------------------------------------------------------------------------

if os.path.isfile(descarga_local) and os.path.isfile(nuevo_destino_copia):
    if filecmp.cmp(descarga_local, nuevo_destino_copia, shallow=False):
        print(f' PASO 4A -->  Los ficheros CSV {descarga_local} y {nuevo_destino_copia} son iguales')
    else:
        print(f' PASO 4A -->  Los ficheros CSV {descarga_local} y {nuevo_destino_copia} son diferentes')
else:
    print(f' PASO 4A --> No existen los dos ficheros CSV para compararlos')

# comparo los ficheros tsv
if os.path.isfile(destino_tsv) and os.path.isfile(nuevo_tsv):
    if filecmp.cmp(destino_tsv, nuevo_tsv, shallow=False):
        print(f' PASO 4B --> Los ficheros TSV {destino_tsv} y {nuevo_tsv} son iguales')
    else:
        print(f' PASO 4B --> Los ficheros TSV {destino_tsv} y {nuevo_tsv} son diferentes')
else:
    print(f' PASO 4B --> No existen los dos ficheros tsv para compararlos')

# --------------------------------------------------------------------------------
# descarga xlsx de url a fichero local
# --------------------------------------------------------------------------------
# definicion de variables
url_xlsx_descarga = 'https://datos.madrid.es/egob/catalogo/300110-23-accidentes-bicicleta.xlsx'  # url descarga
f_xlsx_local = 'BICI_XLSX_LOCAL.xlsx'  # nombre fichero en local
ruta = os.getcwd() + os.sep  # directorio local
descarga_xlsx_local = ruta + f_xlsx_local

# llamo al metodo para descargar el fichero xlsx de la url a un directorio local
descarga_xlsx_a_local(url_xlsx_descarga, descarga_xlsx_local)
print(f" PASO 5A --> Realizada la descarga del xlsx de la URL al fichero LOCAL {descarga_xlsx_local}")
# df_local = pd.read_excel('BICI_XLSX_LOCAL.xlsx')
# imprimir las cinco primeras columnas
# print(df_local.iloc[:, 0:5])

# --------------------------------------------------------------------------------
# Exportar las primeras 5 columnas del fichero en formato .xlsx a un nuevo fichero
# --------------------------------------------------------------------------------
f_xlsx_5COL_local = 'BICI_XLSX_5COL_LOCAL.xlsx'  # nombre fichero en local
descarga_xlsx_5COL_local = ruta + f_xlsx_5COL_local
df2_local = pd.read_excel(descarga_xlsx_local)
df2_local.iloc[:, 0:5].to_excel(descarga_xlsx_5COL_local, index=False)
print(f" PASO 5B --> Creado el fichero {descarga_xlsx_5COL_local} CON 5 COLUMNAS del fichero {descarga_xlsx_local}")
