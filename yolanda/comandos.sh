#! /bin/bash
#lista el contenido del directorio HOME 
#o de un directorio pasado por parametro y que cuelgue de HOME
cd $HOME
if [ $# -eq 0 ]; 
then
	echo "el directorio actual es: $PWD"
	echo "El contenido del directorio es:"
	echo $(ls -a)
else
	echo "El directorio pasado por parametro es: $1"
	if [ -d "$1" ]
	then
		echo "Si existe el directorio"
		echo "El contenido del directorio $1 es $(ls -a $1)"
	else
		echo "NO EXISTE el directorio"
	fi
fi	
