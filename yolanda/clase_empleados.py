# Creando la clase
class Empleado:
    def __init__(self, nombre, apellido1, apellido2, departamento):
        self.nombre = nombre
        self.apellido1 = apellido1
        self.apellido2 = apellido2
        self.departamento = departamento
        self.empresa = "Indra"

    def muestra_datos(self):
        print("Datos del empleado")
        return f"NOMBRE: {self.nombre}\n" \
               f"PRIMER APELLIDO: {self.apellido1}\n" \
               f"SEGUNDO APELLIDO: {self.apellido2}\n" \
               f"DEPARTAMENTO: {self.departamento}\n" \
               f"EMPRESA: {self.empresa}\n"
