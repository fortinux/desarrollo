# -*- coding: utf-8 -*-
import requests  # para obtener la informacion de la web
import json  # para tratar los datos
import os


# metodo para descargar el fichero csv de la url a un fichero local
def descarga_json_a_local(url, fich_local):
    resp = requests.get(url)
    with open(fich_local, 'wb') as output:
        output.write(resp.content)


url_descarga = 'https://datos.madrid.es/egob/catalogo/210227-0-piscinas-publicas.json'  # url descarga
f_json_local = 'piscinas.json'  # nombre fichero en local
ruta = os.getcwd() + os.sep  # directorio local
descarga_local = ruta + f_json_local

# llamo al metodo para descargar el fichero json de la url a un directorio local
descarga_json_a_local(url_descarga, descarga_local)
print(f" PASO 0 --> Realizada la descarga del JSON de la URL al fichero LOCAL {descarga_local}")

# --------------------------------------------------------------------------------
# paso el contenido del json a una variable tipo diccionario
# --------------------------------------------------------------------------------
with open(descarga_local, 'rb') as f:
    dicc = json.load(f)  # almacena fichero json en un diccionario python
    # print(dicc)  #muestra el contenido diccionario completo

claves = list(dicc.keys())
print(f'Las claves del diccionario son:{claves}')

print('\n\t**********primer item ****************')
# valores primer item
print(list(dicc.items())[1][1][0])  # primer elemento de la segunda clave, @graph
# [1] primer elemento  [1] segunda clave [0] primera subclave de la segunda clave

# imprime valores concretos
print("\n\t**********Valores de la PRIMERA CLAVE *************")
print("c -->", dicc['@context']['c'])  # "c": "http://www.w3.org/2002/12/cal#",

print("\n\t ***********Valores de la SEGUNDA CLAVE *******************")
print("Primer Nivel de subclave")
print("id -->", dicc['@graph'][0]['id'])
print("address -->", dicc['@graph'][0]['address'])
print("\n Segundo Nivel de subclave")
print("address:locality -->", dicc['@graph'][0]['address']['locality'])
print("address:postal-code -->", dicc['@graph'][0]['address']['postal-code'])
print("organization:organization-name -->", dicc['@graph'][0]['organization']['organization-name'])

# Buscar datos concretos en los valores de una clave
print("\n\t *********** LOCALIZAR VALORES *************")
cp = str(input("Introduzca codigo postal para localizar los centros deportivos:"))
for valor in dicc['@graph']:
    if valor['address']['postal-code'] == cp:
        print(valor['address']['postal-code'],
              " -->", valor['organization']['organization-name'],
              " -->", valor['address']['street-address'])
