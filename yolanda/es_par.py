num = int(input("Indique un número entero:"))
# un numero es par si es múltiplo de 2
if num % 2 == 0:
    print('El número', num, 'es par.')
else:
    print('El número', num, 'es impar.')
# Si el residuo es 0, es múltiplo
multiplo = 4
if num % multiplo == 0:
    print('El número', num, 'es múltiplo de', multiplo)
else:
    print('El número', num, 'NO es múltiplo de', multiplo)
