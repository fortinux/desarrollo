#! /bin/bash
#el script identifica los parametros especiales de bash

echo "Estos son los parametros especiales de bash"
echo "El nombre del script es: $0"
echo "El PID del shell es: $$"

#compruebo si hay parametros de entrada
if [ $# -gt 0 ];
then
	echo "El numero de parametros de entrada es: $#"
	echo "El listado de parametros de entrada es: ($*)"
	echo "El primer parametro es: $1"
	echo "El ultimo parametro es: ${!#}"
	i=1
	for param in "$@";
	do
		echo "parametro_$i": $param
		i=$(($i+1))
	done
else
	echo "no hay parametros de entrada al script"
fi

#compruebo si la ejecucion del ultimo comando es correcta
ok=$?

if [ $ok -eq 0 ]; then
	echo "Ejecucion correcta"
else
	echo "Ejecucion erronea"
fi


