# -*- coding: utf-8 -*-
import os
import requests
#import xml.etree.ElementTree as ET
from xml.etree import ElementTree as ET


# metodo para descargar el fichero csv de la url a un fichero local
def descarga_xml_a_local(url, fich_local):
    resp = requests.get(url)
    with open(fich_local, 'wb') as output:
        output.write(resp.content)


url_descarga = 'https://datos.madrid.es/egob/catalogo/300227-0-grua-depositos.xml'  # url descarga
f_xml_local = 'GRUAS.xml'  # nombre fichero en local
ruta = os.getcwd() + os.sep  # directorio local
descarga_local = ruta + f_xml_local

# llamo al metodo para descargar el fichero json de la url a un directorio local
descarga_xml_a_local(url_descarga, descarga_local)
print(f" PASO 0 --> Realizada la descarga del XML de la URL al fichero LOCAL {descarga_local}")

# CARGA EL ARBOL
with open(descarga_local, 'rt', encoding='utf-8') as f:
    tree = ET.parse(f)
print(tree)

raiz = tree.getroot()
print(f'El TAG de la raiz es:{raiz.tag}')

print(f'la Raiz tiene: {len(raiz)} hijos')

primer_hijo = raiz[0]
print(f'el TAG del primer hijo:{primer_hijo.tag}')  # infoDataset
print(f'El TEXT del hijo del primer hijo es: {primer_hijo[0].text}')  # Depósitos de la grúa  del Ayuntamiento de Madrid
segundo_hijo = raiz[1]
print(f'el TAG del segundo hijo:{segundo_hijo.tag}')  # contenido
segundo_bisnieto = raiz[1][1][1].text
print(f'bisnieto {segundo_bisnieto}')  # Depósito de la Grúa. Base de Colón
seg = segundo_hijo[1][1].text
print(f'seg:{seg}')  # Depósito de la Grúa. Base de Colón


# imprime las propiedades de los nodos
print("IMPRIME NODOS")
for node in tree.iter():
    print(f' TAG:{node.tag}, TEXT:{node.text}, TAIL:{node.tail}, ATRRIB:{node.attrib}')


print('CREA FICHERO NUEVO MODIFICADO')
for contenido in raiz.findall('contenido'):
    if contenido.find('atributos'):
        atributo = contenido.find('atributos').find('atributo')
        atributo.text = '33333' + atributo.text
tree.write('GRUAS_MODIF_33333.xml', encoding='UTF-8')

