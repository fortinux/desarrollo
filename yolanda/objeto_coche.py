from clase_coche import Coche
from colorama import Fore, Back, Style

# Creando las instancias de la clase Coche
objeto = Coche('Honda', 'Accord', 'Gris')
# Acceder a los atributos de ese objeto

print(Fore.RED+f"Su coche es un: {objeto.marca} modelo:{objeto.modelo} de color: {objeto.color}")
print(Fore.YELLOW+f"Su nivel de combustible es: {objeto.nivel_combustible}")
print(Style.RESET_ALL)
objeto.menu()
