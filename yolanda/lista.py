# defino una lista vacía
lista_ini = []
lista_final = []
y = 7
print(f"Generamos una lista de {y} elementos")
# Inserto y elementos en la lista con un bucle for
for x in range(y):
    # el bucle comienza en 0 por eso indico x+1
    print(f"Elemento {x+1} de la lista:")
    lista_ini.append(input())
print(f"Lista_inicial de {len(lista_ini)} elementos.Su contenido es:{lista_ini}")
# Inserto en una nueva lista el primer y el ultimo elemento de la lista inicial
lista_final.append(lista_ini[0])
lista_final.append(lista_ini[len(lista_ini)-1])
print(f"La lista_final tiene {len(lista_final)} elementos.Su contenido es:{lista_final}")
# otra forma
lista2 = []
lista2.extend([lista_ini[0], lista_ini[len(lista_ini)-1]])
# print(f"Lista2:{(lista2)}")
