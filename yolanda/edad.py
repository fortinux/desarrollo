import datetime
nombre = input("Indique su nombre:")
edad = int(input("Indique su edad:"))
# 100-edad --> calcula el numero de años que le faltan para tener 100 años
# resultado: al año actual le sumo los años que le faltan para tener 100
anos = (100-edad)+int(datetime.datetime.utcnow().strftime("%Y"))
print(f"{nombre} tiene {edad} años. En el año {anos} tendrá 100 años")
# print(datetime.datetime.utcnow()) --> 2021-03-25 18:03:54.845553
