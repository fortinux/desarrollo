#! /bin/bash
#definficion de nombres de ficheros
fich_in=fichero_exp.txt
fich_out1=direcciones_email.txt
fich_out2=direcciones_web.txt
#definicion de directorios
dir_entrada=/home/ysvalverde/fich_entrada
dir_salida=/home/ysvalverde/fich_salida
dir_script=/home/ysvalverde/scripts

echo "Directorio de fichros de entrada: " $dir_entrada
echo "Directorio de ficheros de salida: " $dir_salida
echo "directorio de scripts : " $dir_script
#me posiciono en el directorio donde esta el fichero de entrada
cd $dir_entrada
echo "El fichero de entrada es: " $fich_in
#buscar las direcciones de correo y enviarlas al fichero direcciones_email.txt
#busco las lineas que contengan el caracter @ y las envio al fich_out1
grep @ $fich_in > $dir_salida/$fich_out1
ok1=$?
if [ $ok1 -eq 0 ] 
then
	echo "Creado OK " $fich_out1
else
	echo "error al crear " $fich_out1
fi
#buscar las direcciones web y enviarlas al fichero direcciones_web.txt
#busco las lineas que comiencen por http y las envio al fich_out2
grep ^http $fich_in > $dir_salida/$fich_out2
ok2=$?
if [ $ok2 -eq 0 ] 
then
	echo "Creado OK " $fich_out2
else
	echo "error al crear " $fich_out2
fi
