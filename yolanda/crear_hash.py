# -*- coding: utf-8 -*-
import hashlib
import os

ruta = os.getcwd() + os.sep  # directorio local
nom_fich = 'Ejercicio_python_clase3.py'
# Se crea una variable con el fichero a ser usado para el hash
f_local = ruta + nom_fich
print(f'El fichero local es:{f_local}')
# D:\CURSO_2021\python_projects\prueba2\Ejercicio_python_clase3.py
# Se convierte el formato a bytes usando 'encode'
# Las funciones hash solamente aceptan esta codificación
f_codif = f_local.encode()  # default 'utf-8'
# Se le pasa nombre_codificado a la función sha256
f_hash = hashlib.sha256(f_codif)
# Ya creado el hash, se imprime la versión hexadecimal del mismo usando el método 'hexdigest()'
print("Object:", f_hash)
print("Hexadecimal format:", f_hash.hexdigest())

