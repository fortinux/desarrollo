from clase_empleados import Empleado
from colorama import init, Fore, Back, Style
init()


class Aptitudes(Empleado):
    def __init__(self, nombre, apellido1, apellido2, departamento):
        super().__init__(nombre, apellido1, apellido2, departamento)
        # Nuevos atributos
        self.lenguajes = []
        self.so = []
        self.idioma = []

    def inserta_lenguajes(self):
        num_lenguajes = int(input("Indique el número de lenguajes de programación que conoce:"))
        for x in range(num_lenguajes):
            print(f"Introduzca el lenguaje {x + 1}:", end=" ")
            self.lenguajes.append(input())

    def inserta_sistemas_operativos(self):
        num_sistemas = int(input("Indique el número de sistemas operativos que conoce:"))
        for x in range(num_sistemas):
            print(f"Introduzca el sistema operativo {x + 1}:", end=" ")
            self.so.append(input())

    def inserta_idiomas(self):
        num_idiomas = int(input("Indique el número de idiomas que conoce:"))
        for x in range(num_idiomas):
            print(f"Introduzca el idioma {x + 1}:", end=" ")
            self.idioma.append(input())

    def muestra_lenguajes(self):
        print(f"Lenguajes de programacion:{self.lenguajes}")

    def muestra_sistemas_operativos(self):
        print(f"Sistemas operativos:{self.so}")

    def muestra_idiomas(self):
        print(f"Idiomas:{self.idioma}")
        # for element in self.idioma:
        # print(element)

    def muestra_datos(self):
        print(f"Recogida de datos para el CV")
        self.inserta_lenguajes()
        self.inserta_sistemas_operativos()
        self.inserta_idiomas()
        print(Fore.LIGHTBLUE_EX + "Datos y aptitudes del empleado")
        print(f"NOMBRE: {self.nombre}\n"
              f"PRIMER APELLIDO: {self.apellido1}\n"
              f"SEGUNDO APELLIDO: {self.apellido2}\n"
              f"DEPARTAMENTO: {self.departamento}\n"
              f"EMPRESA: {self.empresa}\n")
        print(Fore.GREEN)
        self.muestra_lenguajes()
        self.muestra_sistemas_operativos()
        self.muestra_idiomas()
        print(Style.RESET_ALL)


# probando la clase empleado
print(f"CLASE EMPLEADO")
Emp1 = Empleado('María', 'López', 'Ramos', 'Energía')
print(Fore.RED+Emp1.muestra_datos())

# prueba 2 con la clase aptitudes
print(Style.RESET_ALL)
print(f"CLASE APTITUDES")
Emp2 = Aptitudes('Elena', 'Alvarez', 'Rodriguez', 'Telco')
Emp2.muestra_datos()
