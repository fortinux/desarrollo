from getpass import getpass
import scp
import paramiko

host = 'IP_del_servidor'
user = 'usuario'

if __name__ == '__main__':
    try:

        cliente = paramiko.SSHClient()
        # Se define utilizar la credencial del usuario
        cliente.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        password = getpass("Ingrese su clave ")
        cliente.connect(host, username=user, password=password)

        scp_cliente = scp.SCPClient(cliente.get_transport())
        # # EJEMPLOS PARA SUBIR O DESCARGAR FICHEROS
        # # UTILIZANDO SCP
        # # DESCOMENTAR UNO O AMBOS A LA VEZ
        # # Subir fichero
        # scp_cliente.put(
        #     'fichero_paramiko.py',
        #      '/home/usuario/fichero_paramiko.py'
        # )

        # # Descargar fichero
        # scp_cliente.get(
        #     'fichero_paramiko.py',
        #     '/home/usuario/'
        # )

        # Crear una sesión remota utilizando un canal
        session = cliente.get_transport().open_session()
        if session.active:
            session.exec_command('cd /home/usuario/ && ls -la')
            # Obtener la salida del comando
            resultado = session.recv(1024).decode()
            print(resultado)

        scp_cliente.close()
        cliente.close()

    except paramiko.ssh_exception.AuthenticationException as e:
        print("Error de autenticación")
