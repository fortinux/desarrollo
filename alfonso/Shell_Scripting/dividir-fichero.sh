#!/bin/bash

# Este script recibe como parámetro un fichero que contiene tanto direcciones email como
# direcciones web, lo procesa y genera dos ficheros: direcciones_email.txt para las direc-
# ciones de correo y direcciones_web.txt para las direcciones web.

if [ -f $1 ]; then
	echo " "
	printf "Dividiendo fichero \"$1\"\n\n"
	grep -i "@" $1 > direcciones_email.txt
	grep -i "://" $1 > direcciones_web.txt
	echo " "
	echo "Proceso finalizado con éxito"
	echo " "
else
	printf "\"$1\" no se he encontrado el fichero indicado\n"
fi
