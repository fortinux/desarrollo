#!/bin/bash
echo "Se han pasado $# parámetros desde línea de comando"
echo "Esos parámetros pasados han sido $@"
echo "El primer parámetro era: $1"
echo "El último parámetro era: $6"
echo "El script se llama $0"
echo "Ejecuto a continuación pwd"
pwd
echo "pwd devolvió $?"
echo "Este script tiene como ID de proceso $$"
echo "El script lo ha ejecutado $USER"
echo "$USER está ejecutando Linux Ubuntu en $HOSTNAME"
echo "Espera 3 segundos, por favor..."
sleep 3
echo "El script se ha estado ejecutando durante $SECONDS segundos"
echo "Esta es la línea $LINENO del script"
echo " "
echo "--- FIN ----"
