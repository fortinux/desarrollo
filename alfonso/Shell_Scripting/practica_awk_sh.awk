
# Este script realizado exclusivamente con comandos AWK realiza una lectura de las líneas del
# fichero /etc/passwd, mostrando únicamente las columnas 1, 3, 4 y 5 de dicho fichero y un
# recuento del número total de líneas que lo conforman.

awk 'BEGIN { print "Visualización del fichero /etc/passwd." }'
awk -F: '{ print $1,$3,$4,$5 }' /etc/passwd
awk 'END { print "|----------- Número de líneas fichero /etc/passwd ---------> " NR }' /etc/passwd 

