#!/bin/bash
clear

SALIR=0
OPCION=0

while [ $SALIR -eq 0 ]; do
	echo
	echo "Seleccione una de las siguientes opciones:"
	echo
	echo "1) Usuarios logueados"
	echo "2) Logins no realizados"
	echo "3) Configuración de red"
	echo "4) Tabla de enrutamiento"
	echo "5) Servicios de red activos"
	echo "6) Mensajes del sistema"
	echo "7) SALIR"
	echo
	echo
	echo "Opcion seleccionada: "
	read OPCION
	case $OPCION in
	 1)
		echo "Opcion 1 seleccionada. Pulse Intro para continuar"
		clear
		echo
		who -all -H
	        echo
		;;
	
	 2)
		echo "Opcion 2 seleccionada. Pulse Intro para continuar"
		clear
		echo
		lastlog --time 30
		echo
		echo
		utmpdump /var/log/btmp
		;;
	 3)
		echo "Opción 3 seleccionada. Pulse Intro para continuar"
		clear
	  	echo
		ifconfig
		echo
		;;
	 4)
		echo "Opción 4 seleccionada. Pulse Intro para continuar" 
		clear
		echo
		route -e
		echo
		;;
	 5)
		echo "Opción 5 seleccionada. Pulse Intro para continuar" 
		clear
		echo
		systemctl list-units -all --type=service --no-pager | grep network | grep loaded
		echo
		echo
		systemctl list-units -all --type=service --no-pager | grep network | grep loaded | grep running
		;;
	 6)
		echo "Opción 6 seleccionada. Pulse Intro para continuar" 
		clear
		dmesg -l err,warn
		;;
	 7)
		echo "Opción 7 SALIR seleccionada. Pulse Intro para salir"
		read OPCION7
			 SALIR=1 ;;
	 *)
		echo "Opción errónea" ;;
	esac 
done
