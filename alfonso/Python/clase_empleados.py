# Creación de la clase Empleados:

class Empleados:

# Definición de las propiedades de la clase Empleados. Por herencia estas propiedades las van a tener
# todos los elementos que se definan dentro de esta clase Empleados.

    def __init__(self, prop_nombre, prop_apellidos, prop_num_empl):
        self.nombre = prop_nombre
        self.apellidos = prop_apellidos
        self.num_empl = prop_num_empl

# Creación de los objetos.

objeto_empleado = Empleados('Alfonso','Gómez Ledesma','300865')
objeto_empleado2 = Empleados('Alejandro','Pérez López','311067')

print(objeto_empleado.nombre)
print(objeto_empleado.apellidos)
print(objeto_empleado.num_empl)

# Creación de la subclase Aptitudes

class Aptitudes(Empleados):

# Definición de las propiedades de la clase Aptitudes.

    def SO_Win(self, ok):
        self.ok="Tiene conocimientos del S.O. Windows."

    def SO_Linux(self, ok2):
        self.ok2="Tiene conocimientos del S.O. Linux."

    def SO_MacOS(self, ok3):
        self.ok3="Tiene conocimientos del S.O. Machintosh."

    def LP_Python(self, ok4):
        self.ok4="Tiene conocimientos del lenguaje programación Python."

    def LP_Java(self, ok5):
        self.ok5="Tiene conocimientos del lenguaje programación Java."

    def ENG_1(self, ok6):
        self.ok6="Tiene conocimientos de inglés básicos."

    def ENG_2(self, ok7):
        self.ok7="Tiene conocimientos de inglés medio."

    def ENG_3(self, ok8):
        self.ok8="Tiene conocimientos de inglés alto."

    def estado(self):
        print("Nombre: ", self.nombre, "\nApellidos: ", self.apellidos, "\nAptitudes: ", self.ok, self.ok2, self.ok3)
        print (self.ok4, self.ok5)
        print (self.ok6, self.ok7, self.ok8)

apts=Aptitudes("Alfonso", "Gómez Ledesma", "300865")
apts.SO_Win("ok")
apts.SO_Linux("ok2")
apts.SO_MacOS("ok3")
apts.LP_Python("ok4")
apts.LP_Java("ok5")
apts.ENG_1("ok6")
apts.ENG_2("ok7")
apts.ENG_3("ok8")
apts.estado()

print("")
print(objeto_empleado2.nombre)
print(objeto_empleado2.apellidos)
print(objeto_empleado2.num_empl)

apts=Aptitudes("Alejandro", "Pérez López", "311067")
apts.SO_Win("ok")
apts.SO_Linux("ok2")
apts.SO_MacOS("ok3")
apts.LP_Python("ok4")
apts.LP_Java("ok5")
apts.ENG_1("ok6")
apts.ENG_2("ok7")
apts.ENG_3("ok8")
apts.estado()