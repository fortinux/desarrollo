num = int(input("Introduzca un número entero: "))
if num < 0:
    num = 0
    print('Ha introducido un número negativo, no válido')
    exit()
elif num == 0:
    print('Ha introducido un 0')
    exit()

resto = (num % 2)
if resto == 0:
    print('El número introducido es par')
else:
    print('El número introducido es impar')

# Múltiplo de 4

multi4 = (num % 4)
if multi4 == 0:
    print('El número introducido es múltiplo de 4')
else:
    print('El número introducido no es múltiplo de 4')