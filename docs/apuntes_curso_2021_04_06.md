**Escribir la declaración XML al exportar a un fichero en Python **  
    
*xml.etree.ElementTree.tostring escribe la codificación XML con encoding='utf8'. Un ejemplo :  
   
```
import xml.etree.ElementTree as ElementTree

tree = ElementTree.ElementTree(
    ElementTree.fromstring('<xml><test>123</test></xml>')
)
root = tree.getroot()

print('without:')
print(ElementTree.tostring(root, method='xml'))
print('')
print('with:')
print(ElementTree.tostring(root, encoding='utf8', method='xml'))

```
    
Extraído de:  
https://stackoverflow.com/questions/15356641/how-to-write-xml-declaration-using-xml-etree-elementtree  
  
   
**Convertir una cadena de texto a un número entero o con coma flotante**  

```
>>> a = "3.1415"
>>> float(a)
3.1415
>>> int(float(a))
3

```
    
Recurso: https://stackoverflow.com/questions/379906/how-do-i-parse-a-string-to-a-float-or-int?rq=1  
    
     
**Diferencias entre sys.platform, os.name y platform.system**  
La diferencia es que los dos primeros determinan su salida en tiempo de compilación, en cambio platform.system lo hace en tienmpo de ejecución.  
    
Fuentes:  
https://stackoverflow.com/questions/4553129/when-to-use-os-name-sys-platform-or-platform-system    
https://stackoverflow.com/questions/1854/python-what-os-am-i-running-on/58071295#58071295    
https://docs.python.org/es/3/library/platform.html    



**Uso del caracter b delante de un string en Python**    
Los literales son notaciones para los valores constantes de algunos tipos incorporados, en este caso "b" identifica a los literales en Bytes, al contrario de un string.
    
Fuentes:    
https://docs.python.org/es/3/reference/lexical_analysis.html#string-and-bytes-literals    
https://stackoverflow.com/questions/6269765/what-does-the-b-character-do-in-front-of-a-string-literal    
    
    
