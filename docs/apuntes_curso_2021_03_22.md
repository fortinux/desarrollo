**Apuntes Python para sysadmins**  
   
**PyCharm**    
    
Ejemplo de cómo clonar (hacer una copia sincronizada) de un repositorio remoto en la máquina local.
   
*Menú File > New Project*: Seleccionar directorio y clicar en *create*.
   
Al abrirse el ambiente virtual del nuevo proyecto:   
    
*Menú VCS > Get from version control*: En el campo URL escribir el repositorio a clonar:  
https://bitbucket.org/fortinux/desarrollo/src/master/
Finalmente clicar en *clonar*.   
    
PyCharm creará la copia local permitiéndonos modificar los ficheros para subirlos con un *push* al repositorio remoto.
    
Fuente:    
https://www.jetbrains.com/help/pycharm/set-up-a-git-repository.html#put-existing-project-under-Git
     
            *************** . *****************    

**Pycharm: configurar un repositorio local en Bitbucket**   
Si ya tenemos un proyecto en nuestra máquina local y queremos subirlo a un repositorio nuevo en Bitbucket seguiremos los siguientes pasos:   
    
En Bitbucket crear un repositorio vacío y clicar en clonar para copiar la dirección web del mismo.

En el ambiente virtual del proyecto local:   
    
*Menú VCS > Enable version control integration...*: Se selecciona Git y luego clicar en *ok*   

*Menú VCS > Get from version control*: En el campo URL escribir el repositorio a clonar:  
https://bitbucket.org/...
Finalmente clicar en *clonar*.   
    
Al hacer pull o push PyCharm sincronizará la copia local con la del repositorio remoto.
    
Fuente:    
https://www.jetbrains.com/help/pycharm/set-up-a-git-repository.html#add-remote   

IMPORTANTE:   
Es necesario configurar el fichero .gitignore con los ficheros que por conveniencia y medidas de seguridad no deben ser compartidos.
Se puede descargar un fichero modelo de https://github.com/github/gitignore/blob/master/Global/JetBrains.gitignore 
    
Fuente:     
https://intellij-support.jetbrains.com/hc/en-us/articles/206544839   
    