**Herramientas de configuración de servidores:**

Webmin https://www.webmin.com  
Webmin es una herramienta de configuración de sistemas accesible  
vía web para sistemas Unix, como GNU/Linux y OpenSolaris. 

Cockpit https://cockpit-project.org  
Preinstalado en CentOS 8, solo es necesario habilitarlo.  
```
sudo systemctl enable --now cockpit.socket
sudo firewall-cmd --permanent --zone=public --add-service=cockpit
sudo firewall-cmd --reload
```
En un navegador nos conectamos https://IP_servidor:9090  
Entramos con nuestro nombre de usuario y clave (debe tener permisos   de root, caso contrario es necesario crear uno nuevo.   No se aconseja utilizar root como usuario por motivos de seguridad.)

ISPCONFIG https://www.ispconfig.org/  
Gestión de múltiples servers; SO GNU/Linux Debian, Ubuntu, CentOS.  

**Instalación de entornos gráficos:**

// Debian - Ubuntu  
```
sudo apt-get install lxde
sudo apt install gnome-session gdm3
```

// CentOS  
` sudo yum groupinstall "Server with GUI" `

**Escritorio remoto:**  
Remmina https://remmina.org/  
TigerVNC https://tigervnc.org/  
vinagre https://wiki.gnome.org/Apps/Vinagre  

Privativos:  
https://www.teamviewer.com/  
https://remotedesktop.google.com/  

