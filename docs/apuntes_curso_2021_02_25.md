**Gestión de red en CentOS 8**
CentOS 8 gestiona la red con NetworkManager. Viene ya instalado pero en caso de ser necesario se instala y verifica si esá activo con:

```
dnf install NetworkManager

systemctl is-active NetworkManager
systemctl is-enabled NetworkManager
systemctl status NetworkManager 
```

Las herramientas para la gestión son `nmtui` eo en la ínea de comandos:

`nmcli`

Los ficheros de configuracón de las interfases se encuentran en el directorio `/etc/sysconfig/network-scripts/`.

Se puede encontrar más información sobre NetworkManager en:  
How to Manage Networking with NetworkManager in RHEL/CentOS 8  
https://www.tecmint.com/manage-networking-with-networkmanager-in-rhel-centos/   
OMG, CentOS 8 no longer supports systemd-networkd  
https://www.reddit.com/r/CentOS/comments/jx1eji/omg_centos_8_no_longer_supports_systemdnetworkd/ 

