**Apuntes Shell scripts**

Si queremos poner un comando en un shell script que necesite permisos de *root* sin solicitarle una password al usuario, 
debemos editar */etc/sudoers* agregando los permisos para que pueda ejecutarlo:  
  
```
sudo visudo
# Agregamos la(s) línea(s) 
usuario ALL = (ALL) ALL
usuario ALL = (root) NOPASSWD: /ruta/al/script

```
  
Fuente:

https://unix.stackexchange.com/questions/18830/how-to-run-a-specific-program-as-root-without-a-password-prompt   

**Crear shell script para apagar la maquina:**   

```
#!/bin/bash

# """
# [Este script apaga la maquina en la cual se ejecuta]
# [Basado en los comentarios de https://superuser.com/users/293183/biapy ]
# Author: Fortinux
# Date: [2021/03/18]
# """

# averiguar la ruta al comando. En SO GNU/Linux debería ser /sbin/shutdown
# command -v shutdown
# 

/sbin/shutdown -h now

```
     
**"Goto" en Shell Scripts:**  
No existe un "goto" en Shell Scripts aunque se pueden implementar soluciones de contorno ("apaños :-) utilizando *if, 
case*, o creando una función. No olvidar de todas maneras que existen *continue* y *break* para controlar la ejecución 
de los loops.  

Fuentes:   
https://stackoverflow.com/questions/9639103/is-there-a-goto-statement-in-bash  
https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Compound-Commands  
     
**Windows Command Prompt Cheatsheet**  
Para el futuro curso de Python, quienes tenéis SO Windows: chuletillas de comandos básicos del CMD de Windows y de 
Powershell.     
  
Recursos:  
http://www.cs.columbia.edu/~sedwards/classes/2015/1102-fall/Command%20Prompt%20Cheatsheet.pdf   
https://download.microsoft.com/download/2/1/2/2122F0B9-0EE6-4E6D-BFD6-F9DCD27C07F9/WS12_QuickRef_Download_Files/PowerShell_LangRef_v3.pdf  

**Cómo colorear el output de la Terminal en Linux**  
Un excelente tutorial aportado por Yolanda:  
  
https://robologs.net/2016/03/31/como-colorear-el-output-de-la-terminal-en-linux/  
