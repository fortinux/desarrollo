**Apuntes Curso Python: PyCharm Básico**

Áreas en PyCharm:  
  
- Barra de menús y barra de herramientas.
- Proyectos, estructura y favoritos a la izquierda de la pantalla; en el centro el fichero a editar; y a la derecha una previsualización del mismo.  
- En la parte inferior de la pantalla las secciones TODO (para hacer), la sección de problemas, una shell para ejecutar comandos y la consola de Python. 
  A su lado está la ventana de eventos con los logs. 
  Si estamos conectados a un repositorio nos aparece también la ventana de Git con los commits y cambios locales entre otras cosas.
- La barra de estado de forma predeterminada nos muestra el último evento del log, el file encoding, indentado, intérprete de Python utilizado, y la rama del repositorio actual.
  
 
Menú File > Settings > Project > Python Interpreter > Add module > Install package (Marketplace)  
Menú File > Settings > Appearance and behavior > Theme  
Menú File > Settings > Editor > Color scheme  
Menú File > Settings > Plugins > Marketplace > Presentation assistant 

Atajos del teclado
#TODO actualizar fisher
```
CTRL + ALT + SHIFT + INSERT > crea nuevo fichero
SHIFT + SHIFT > busca por todo PyCharm
CTRL + SHIFT + A > busca por las acciones 
CTRL + ALT + SHIFT + N > busca clase, variables, etc.
CTRL + E > muestra ficheros abiertos recientemente
ALT + 1 > abre ventana proyectos
ALT + 2 > abre favoritos
ALT + 7 > structure
F2 > errores
F5 > copia fichero
F6 > mueve fichero
CTRL + SHFT + F > buscar en fichero
CTRL + SHIFT + R > reemplazar en fichero
CTRL + / > Comenta un bloque de código
SHIFT + F6 > Cambia todas las ocurrencias simultáneamente
CTRL + SHIFT + F10 > Ejecutar por primera vez un fichero .py
CTRL + ALT + I > Autoindentar la selección
SHIFT + TAB > Elimina espacios
    
Extensiones   
Python Debugger Extension Available : Cython extension    
   

```
    
Fuente:    
     
https://www.jetbrains.com/pycharm/guide/    
     
