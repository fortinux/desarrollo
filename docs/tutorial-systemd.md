**Crear un servicio con systemd:**  
En este ejemplo se creará en sistemas operativos GNU/Linux un   servicio con systemd que ejecute el servidor SimpleHTTPServer   escrito en Python en el puerto 8000 del servidor local.  

*Por motivos de seguridad este servidor NO debería ejecutarse  
en ambientes de producción.*  

Los pasos para crear un servicio en Systemd son los siguientes:  

1) Abrimos un terminal y se crea el fichero con las instrucciones para Systemd del nuevo servicio:  

```
vim /lib/systemd/system/ejemplo.service 

[Unit]
Description=Ejemplo
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=usuario
ExecStart=/usr/bin/env python3 /usuario/ejemplo.py

[Install]
WantedBy=multi-user.target
```

Aquí se debe cambiar el nombre del usuario y la ruta al script en  python con vuestros datos. Salvamos y guardamos los cambios.  

2) En el directorio /usuario/ (reemplazar la ruta con la escrita en el fichero anteriormente creado) programar el siguiente script en python:  

```
vim ejemplo.py

import http.server
import socketserver

PORT = 8000

handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", PORT), handler) as httpd:
    print("Server started at localhost:" + str(PORT))
    httpd.serve_forever()
```

Salvamos y guardamos los cambios.  

3) Iniciamos el servicio, verificamos que funciona correctamente  y lo habilitamos para que se ejecute automáticamente:  

```
sudo systemctl start ejemplo
sudo systemctl status ejemplo
sudo systemctl enable ejemplo
```

Finalmente abrimos un navegador y escribimos la ruta al servidor  SimpleHTTPServer reemplazando la IP con la vuestra:  

http://10.0.2.2:8000/

**Crear un timer con systemd:**
En este ejemplo se creará en sistemas operativos GNU/Linux un timer con systemd que ejecute un script en bash.  

Los pasos para crear un timer en Systemd son los siguientes:  
1) Abrimos un terminal y creamos el fichero con las instrucciones para Systemd del nuevo servicio:  

```
vim /lib/systemd/system/fecha.service 

[Unit]
Description=Una prueba

[Service]
Type=oneshot
ExecStart=/bin/bash /usuario/fecha.sh 
```

Aquí se debe cambiar el nombre del usuario y la ruta al script en  bash con vuestros datos. Salvamos y guardamos los cambios.  

2) En el directorio /usuario/ (reemplazar la ruta con la vuestra  escrita en el fichero anteriormente creado) programamos el  siguiente script en python:  

```
vim fecha.py

#!/bin/bash
FECHA=$(date '+%Y-%m-%d %H:%M:%S'); echo -e "$FECHA" >> /usuario/registro.log
echo -e "Hola mundo" >> /usuario/registro.log
```

Salvamos y guardamos los cambios.  

le damos permisos de ejecución con:  
```
chmod 755 fecha.sh 
```

3) Ahora se debe crear el fichero timer:  

```
vim /lib/systemd/system/fecha.timer 

[Unit]
Description=Runs every 1 minutes fecha.sh

[Timer]
OnCalendar=*:0/1

[Install]
WantedBy=timers.target
```

Salvamos y guardamos los cambios.  


4) Iniciamos el servicio, verificamos que funciona correctamente  
y lo habilitamos para que se ejecute automáticamente:  

```
sudo systemctl start fecha
sudo systemctl status fecha
sudo systemctl enable fecha
```

Si hacemos algún cambio a los ficheros de systemd se debe ejecutar  la siguiente orden:  

```
systemctl daemon-reload
```

Referencias:  

Atareao.es. Como crear un servicio con systemd.  
https://www.atareao.es/tutorial/trabajando-con-systemd/como-crear-un-servicio-con-systemd/  
Wikipedia. systemd. https://es.wikipedia.org/wiki/Systemd  
Tutorial Digital Ocean.  
https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units-es  


