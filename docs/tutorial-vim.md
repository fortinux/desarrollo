**Tutorial básico de Vi / Vim**
_Editando texto en la línea de comando_

_Vi_ fue el primer editor de texto full-screen escrito para Unix.  Está diseñado para ser pequeño y simple. Hay una variante de _Vi_  bastante conocida: _Vim_, o “Vi Improved.”  

Tiene tres modos:  

* Command mode: acepta comandos,
* Ex mode: manipula ficheros. Se entra con *:*,
* Insert mode: para inserir texto en los ficheros.  

Algunos comandos:  
*dd* trabaja como *yy* (copia la línea de texto), pero elimina las  líneas cuando las copia al buffer. Para pegar se escribe *p*  comenzando en la línea después del cursor. Para pegar en la línea  anterior al cursor, usar *P* mayúscula.  

Hay varios comandos para entrar en insert mode:  

* *R* , que reeemplaza el texto seleccionado,
* *i* que inserta el texto en vez de sobreescribirlo, 
* *a* para avanzar el cursor un espacio.

Para salvar el fichero y salir: *:wq*. Este es un comando ex-mode. (El comando *ZZ* es equivalente).  

Deshacer: Para deshacer cualquier cambio, escribir *u* en command mode.  

Open text: en command mode, escribir *o* inserta una nueva línea  inmediatamente debajo de la actual y entra en insert mode.  

Buscar: Para buscar hacia adelante en el texto, escribir */* en  command mode, seguido del texto a localizar.  
En cambio escribir *?* Para buscar hacia atrás.  

Cambiar texto: El comando *c* cambia el texto desde dentro del command mode. Usar *cw* para cambiar a la siguiente palabra o *cc* para cambiar la línea entera.  

Ir a la primera línea del fichero: *gg*
Ir a la última línea: *G*

Reemplazar globalmente: Para reemplazar todas las ocurrencias de una cadena con otra escribir *:%s/original/replacement*.

Editar un nuevo fichero: :*e*  
Incluir un fichero existente: :*r*  

Ejecutar un comando externo: El comando ex-mode *:!* ejecuta un comando externo al igual que escribir *:!ls* ejecuta *ls*, lo que permite ver qué ficheros están presentes en el directorio actual.  

Quit: Usar el comando *:q* para salir del programa. Agregar un símbolo de exclamación al comando para salir sin hacer cambios.  

vim:
*dw* // Borra una palabra a la derecha  
*d$* // Borra hasta el final de la línea  
*dd* // Borra la línea entera  
*p* // Pega  
*u* // Deshace los cambios  
*yy* // Copia la línea. Una *y* copia el párrafo entero  
*ZZ* // Guarda los cambios y sale  

*!vim* // Abre el último fichero sin escribir todo el comando  

Usar *!!* para ejecutar el último comando  

Presionando *CRTL+R* y escribiendo las palabras que tenía en  su último comando, UNIX encontrará el comando. Para ejecutarlo se presiona enter.

*history | grep "lxc"* // Para encontrar el comando *lxc* en history

Fuente: https://hackernoon.com/10-basic-tips-on-working-fast-in-unix-or-linux-terminal-5746ae42d277 
