**LightDM, GDM (GNOME Display Manager), y KDM:**
LightDM, GDM (GNOME Display Manager), y KDM son administradores de inicio de sesión

-Ejecutan X server cuando es necesario.
-Proporcionan inicio de sesión gráfica: la GUI con la lista de usuarios del sistema para permitir elegir qué cuenta y tipo de sesión usar.
-Permiten autenticación mediante PAM.
-Ejecutan procesos de sesión una vez que se completa la autenticación.
-Proporcionan opciones de inicio de sesión gráfico remoto. 

Para reconfigurar gdm:  

```
sudo dpkg-reconfigure gdm 
```

Se puede encontrar más información sobre LightDM en https://github.com/canonical/lightdm
Ayuda de GMD: https://help.gnome.org/admin/gdm/

**i3 tiling window manager**  
i3 está orientado a usuarios avanzados y desarrolladores.  

Referencias:  
Página del proyecto: https://i3wm.org/  
i3 Reference cards: https://i3wm.org/docs/refcard.html  
Reddit i3 Window Manager https://www.reddit.com/r/i3wm/   

**Cronjobs:**

```
2 * * * * tar czf /home/usuario/scripts.tar.gz /home/usuario/scripts
2 * * * * /sbin/ping -c 4 google.com; ls -la >> /home/usuario/cron 2>&1
```

```
ps aux | grep cron 
sudo su -
[root]# sudo grep CRON /var/log/syslog
```

Ver cron logs en CentOS Ubuntu :  

```
less /var/log/cron 
less /var/log/cron.log
```

Habilitar cron jobs logging:  
Para resolver problemas se editan /etc/rsyslog.conf o /etc/rsyslog.d/50-default.conf (en Ubuntu):  

```
sudo vim /etc/rsyslog.d/50-default.conf
cron.*                         /var/log/cron.log
```
Luego se reinician rsyslog y cron:
```
sudo service rsyslog restart
sudo service cron restart
```

Cron jobs se almacenan en /var/log/cron.log.  

Para verificar que un cron está funcionando correctamente se puede crear un crontab que redirija la salida a un fichero de log:  
```
2 * * * * /home/usuario/myscript >> /home/log/myscript.log 2>&1

test cronjob
```

Se agrega temporalmente al crontab:
```
* * * * * env > ~/cronenv
```

Luego se ejecuta:
```
env - `cat ~/cronenv` /bin/sh

```

https://stackoverflow.com/questions/2135478/how-to-simulate-the-environment-cron-executes-a-script-with  
