**Configurar par de llaves SSH para acceder a repositorios Bitbucket:**

En máquina local ejecutar:  
```
ssh-keygen -t rsa -b 4096 -C "mi_nombre@micorreo.com"
```
// Controlar si está ejecutándose  
```
eval "$(ssh-agent -s)"
// Si encontramos un error de autenticación ejecutamos también:
ssh-add
```
// Agregarla al servicio SSH  
```
ssh-add -K /Usuario/mi_nombre/.ssh/id_rsa
```
En el portal de Bitbucket:  
  
Seleccionar -> Personal settings del avatar en la parte inferior izquierda de la ventana -> Clicar en SSH keys      

Add key: Crear una nueva llave pública con la máquina virtual  

En la máquina virtual:   
  
// Verificar que está todo correcto  
```
ssh -T git@bitbucket.org
```
// Ya se puede clonar el repositorio y trabajar con él  
```
git clone git@bitbucket.org:fortinux/desarrollo.git
```
Recursos:  
https://github.com/fortinux/ejemplos-git/blob/master/tutorial_configurar_GIT_linux.md  
https://www.atlassian.com/es/git/tutorials/setting-up-a-repository  


**Addendum**  

Para guardar la autenticación por un cierto período de tiempo:  

```
git config --global credential.helper cache
# Configura git por 15 min. para usar la memoria cache
```

Para cambiar el intervalo por defecto de la memoria caché de la contraseña:  

```
git config --global credential.helper 'cache --timeout=3600'
# Configura el cache a finalizar después de una hora (en segundos)
```
   
   

