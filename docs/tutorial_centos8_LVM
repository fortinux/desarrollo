Tutorial LVM – Gestor de Volúmenes Lógicos en CentOS 8

En este tutorial veremos cómo crear un gestor de volúmenes lógicos – LVM en CentOS 8 desde la línea de comando.  Observaciones: Este tutorial puede servir para otras distribuciones basadas en Red Hat o en Debian haciendo algunas modificaciones. El LVM se creará utilizando dos discos duros de 1G cada uno.

Introducción al gestor de volúmenes lógicos – LVM

El gestor de volúmenes lógicos (Logical Volume Management – LVM) trata a varios discos duros como si fueran  sólo uno. Los volúmenes lógicos son como ficheros en un sistema de ficheros normal; el sistema de ficheros (o volume group, en el caso de LVM) gestiona la alocación de espacios cuando se redimensionan ficheros o volúmenes lógicos. Los LVM se componen de:

• PV: volúmenes físicos,
• VG: grupo de volúmenes (volume group) – grupo de uno o más PV,
• PE: extensiones de VG (phisical extents) – partes de tamaño físico,
• LV: volumen lógico (logical volume) – volumen virtual,
• LE: extensiones de LV (logical extents) – partes de tamaño lógico.

Instrucciones paso a paso para montar el nuevo LVM en CentOS 8

Abrimos un terminal en CentOS 8 si estamos en un ambiente gráfico o utilizamos la línea de comando para ejecutar las siguientes órdenes.

Para mostrar información sobre los discos:

lsblk

Para ver las particiones y configuración:

sudo fdisk -l

Para crear un volumen físico con los dos discos duros:

sudo pvcreate /dev/sdb /dev/sdc

Para crear un grupo de volúmenes:

sudo vgcreate grupovolumen /dev/sdb /dev/sdc

Para mostrar el  grupovolumen creado:

sudo vgdisplay grupovolumen

Para crear un volumen lógico:

sudo lvcreate -L 2G -n volumenlogico grupovolumen

Para verificar la creación:

sudo lvscan
/dev/grupovolumen/volumenlogico

sudo lvdisplay /dev/grupovolumen/volumenlogico

Para listar los componentes LVM y obtener información:

sudo pvs
sudo vgs
sudo lvs

Para crear un sistema de ficheros en el LV creado:

sudo mkfs.ext4 /dev/grupovolumen/volumenlogico

Para poder usar el nuevo volumen se deberá montar antes:

sudo mkdir /mnt/nuevovolumen
sudo mount /dev/grupovolumen/volumenlogico /mnt/nuevovolumen

Para hacer permanente el montaje se agrega la siguiente línea al fichero /etc/fstab:

/dev/grupovolumen/volumenlogico /mnt/nuevovolumen ext4 defaults 0 0

Para mostrar el nuevo volumen montado y operativo:

df -h

Para dar permisos al usuario de escritura:

sudo chown -R /mnt/nuevovolumen/

Bibliografía:

Red Hat Documentation. <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/logical_volume_manager_administration/logical_volumes>.

Red Hat Documentation. <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/4/html/cluster_logical_volume_manager/lvm_cli>

2day Geek. <https://www.2daygeek.com/create-lvm-storage-logical-volume-manager-in-linux/>


