**Apuntes Trabajando con ramas en git Bitbucket**

Git GUI para Windows y MAC:   
  
https://www.sourcetreeapp.com/  
  
No pedir la clave en Ubuntu 20.04
En caso de no poder configurar ssh-add:
     
```
sudo apt-get install libsecret-1-0 libsecret-1-dev
cd /usr/share/doc/git/contrib/credential/libsecret
sudo make
git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret
```
     
Ramas tutorial en Bitbucket:  
      
https://www.atlassian.com/es/git/tutorials/using-branches  
https://github.com/fortinux/ejemplos-git/blob/master/tutorial_ramas-GIT.md    
       
```
git branch rama-de-pruebas
git branch -v
git checkout rama-de-pruebas
touch texto_rama.txt
git add .
git commit -m "nueva rama de ejemplo"
git push origin rama-de-pruebas
```
     
Para borrar la rama:  
     
```
git branch -d rama   
git branch -D rama
```
       
Para borrar la rama en repositorio:
    
```
git push origin --delete rama
```
    
GIT cheatsheet:  
     
https://www.atlassian.com/es/git/tutorials/atlassian-git-cheatsheet     
   
Agregado https://www.gitpod.io para desarrollo online al repositorio    
   
