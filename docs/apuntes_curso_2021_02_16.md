**Systemd: Mostrar todos los timers activos**

```
systemctl list-timers
```
En cron se suele ejecutar en cambio:

`crontab -l` 

o un `ls` en los directorios */etc/cron.daily, /etc/cron.hourly*, etc.

Se puede encontrar más información sobre systemd en:  
https://wiki.archlinux.org/index.php/Systemd_(Espa%C3%B1ol)


**Instalar ShellCheck en CentOS 8**  
Para descubrir en que repositorio se encontraba ShellCheck he utilizado el sitio web:

`https://centos.pkgs.org/7/epel-x86_64/ShellCheck-0.3.8-1.el7.x86_64.rpm.html`

Descargar el repositorio epel-release rpm de CentOS 7:  

```
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```

Instalar el epel-release rpm:  

```
sudo yum install epel-release-latest-7.noarch.rpm

sudo yum repolist
```

Instalar el paquete ShellCheck rpm:

```
yum install ShellCheck
```

Para utilizarlo:

`shellcheck ejemplo.sh`

Página del proyecto: https://www.shellcheck.net/  
 

**Navegar por Internet usando la línea de comandos:**  

Para instalar el navegador lynx:  

`sudo apt-get install lynx`

Otro navegador es w3m browser:  

`sudo apt-get install w3m w3m-img`

Para usarlo:  

`w3m google.com`

Para ver imagenes, abrir o xtem (en el terminal no funciona)
 
Instalar links2:  

`sudo apt-get install links2`

`links2 google.com`

Para ver imágenes:  

`links2 -g https://fortinux.com`

Alfonso ha comentado el más moderno elinks:  

https://blog.desdelinux.net/elinks-mejor-navegador-web-la-terminal-linux/

**Imprimir desde la terminal de Linux**  
Se instala PDF printer:

`sudo apt-get -y install cups-pdf`

Lista de las impresoras activas (se verá la nueva impresora PDF):  

`lpstat -p -d`


Imprimir un pdf usando `echo`:  

`echo "Hola mundo de los PDFs" | lpr -P PDF`

Imprimir un fichero pdf o varios usando el asterisco:  

```
lp ejemplo.pdf
lp *.pdf
```

Convertir un fichero a pdf:  

```
sudo apt-get install enscript
enscript fichero.txt -o - | ps2pdf - salida_fichero.pdf
```

Fuentes:  
https://terokarvinen.com/2011/print-pdf-from-command-line-cups-pdf-lpr-p-pdf/index.html  
https://askubuntu.com/questions/27097/how-to-print-a-regular-file-to-pdf-from-command-line  
