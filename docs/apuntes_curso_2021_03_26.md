**4.4. Las sentencias break, continue, y else en bucles**  
    
*La sentencia break, como en C, termina el bucle for o while más anidado.  
   
Las sentencias de bucle pueden tener una cláusula`!else` que es ejecutada cuando el bucle termina, después de agotar el iterable (con for) o cuando la condición se hace falsa (con while), pero no cuando el bucle se termina con la sentencia break.*  
  
Extraído de:  
https://docs.python.org/es/3/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops  
  
   
**PEP 394 -- The "python" Command on Unix-Like Systems**  
Explica com configurar el comando python debido a las versiones 2 y 3 del lenguaje. De forma predeterminada en sistemas operativos Debian / Ubuntu *python* ejecuta la rama 2 de Python y *python3* ejecuta la rama 3, aunque se puede crear un alias para modificar este procedimiento. 
   
Recurso: https://www.python.org/dev/peps/pep-0394/  
    
**Instalar pandas en Pycharm**  
Abrir el proyecto y seguir estos pasos:  
    
CTRL + ALT +S para abrir las preferencias del proyecto -> GEAR -> Add  
En la ventana abierta si "Existing environment" está seleccionado, marcar "Make available to all projects"  
Clicar OK  
En mi caso ha solicitado instalar "python packaging tools"  
Al volver al entorno de desarrollo e intentar importar el módulo pandas, dirá que no está instalado y permitirá instalarlo desde allí.  
    
Fuente:  
https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#existing-environment  

**Ejercicio importar y exportar ficheros**    
Banco de España: Descarga de archivos con las series temporales  
https://www.bde.es/webbde/es/estadis/ccff/downld.html  
https://www.bde.es/bde/es/areas/estadis/Recomendaciones_aa6262e6c4a6051.html    
descargado el fichero catalogo_cf.csv  
convertido a .tsv con https://onlinecsvtools.com/convert-csv-to-tsv  
    
**Bibliografía complementaria**   
https://ellibrodepython.com/   
https://docs.python-guide.org/   
   

