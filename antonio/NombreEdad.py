from datetime import date

Nombre = input("¿Cómo te llamas? ")
Edad = int(input("Ahora dime la edad "))

FechaHoy = date.today()
YearHoy = FechaHoy.year
Centenario = (YearHoy+100)-Edad

print(f"Encantado {Nombre}. En el año {Centenario} tendrás un siglo :)")
