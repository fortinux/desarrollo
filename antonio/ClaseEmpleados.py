class ClaseEmpleados:
    def __init__(self, numero, nombre, apellido, dni, ano_nacimiento):
        self.numero = numero                   # Número de empleado
        self.nombre = nombre                   # Nombre del empleado
        self.apellido = apellido               # Apellido del empleado
        self.dni = dni                         # DNI del empleado
        self.ano_nacimiento = ano_nacimiento   # Año de nacimiento del empleado

    def jubilacion_empleado(self):             # Calcula el año de jubilación del empleado
        jubilacion = self.ano_nacimiento+65
        return jubilacion
