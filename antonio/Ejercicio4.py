#
# Ejemplo comparar ficheros
#
#import filecmp
#
#compara = filecmp.cmp("ExcelEjercicio3.tsv", "Ejercicio3/ExcelEjercicio3_copia.csv")
#print(f"Resultado comparación de .tsv con la copia .csv {compara}")
#
#compara = filecmp.cmp("ExcelEjercicio3.csv", "Ejercicio3/ExcelEjercicio3_copia.csv")
#print(f"Resultado comparación de .tsv con la copia .csv {compara}")


#
# Ejemplo crear hash para una cadena de caracteres
#
#import hashlib
#
#texto_cifrado = hashlib.new("sha512", b"Paren el mundo que yo me bajo")
#print(f"Cadena cifrada en binario {texto_cifrado.digest()}")
#print(f"Cadena cifrada en hexadecimal {texto_cifrado.hexdigest()}")


#
# Ejemplo crear hash de un fichero
#
#import hashlib

#fichero = "ExcelEjercicio3.xlsx"                  # Se crea una variable con el fichero para el hash
#nombre_codificado = fichero.encode()              # Se convierte el formato a bytes usando 'encode'
#nombre_hash = hashlib.sha512(nombre_codificado)   # Se le pasa nombre_codificado a la funcion sha512

# Una vez creado el hash, se imprime la versión hexadecimal usando el método 'hexdigest()'
#print("Objeto:", nombre_hash)
#print("Hexadecimal formato:", nombre_hash.hexdigest())


#
# Ejemplo manupular JSON
#
#import json

#curso = {}
#curso['asistentes'] = []

#curso['asistentes'].append({
#    'nombre': 'Marcelo',
#    'conocimientos': ['Ubuntu', 'Python'],
#    'tipo_asistente': 'Profesor',
#    'aula': 'A1'})
#curso['asistentes'].append({
#    'nombre': 'Alfonso',
#    'conocimientos': ['Windows', 'SQLServer'],
#    'tipo_asistente': 'Alumno',
#    'aula': 'B1'})
#curso['asistentes'].append({
#    'nombre': 'Antonio',
#    'conocimientos': ['Oracle', 'Java'],
#    'tipo_asistente': 'Alumno',
#    'aula': 'C2'})
#curso['asistentes'].append({
#    'nombre': 'Mercedes',
#    'conocimientos': ['Red Hat', 'Cobol'],
#    'tipo_asistente': 'Alumno',
#    'aula': 'A1'})
#curso['asistentes'].append({
#    'nombre': 'Pere',
#    'conocimientos': ['Solaris', 'Cisco'],
#    'tipo_asistente': 'Alumno',
#    'aula': '95'})
#curso['asistentes'].append({
#    'nombre': 'Yolanda',
#    'conocimientos': ['CentOS', 'PHP'],
#    'tipo_asistente': 'Alumno',
#    'aula': 'A1'})

#with open("curso.json", 'w') as file:
#    json.dump(curso, file, indent=3)

#print("LISTA DE ASISTENTES AL CURSO\n")

#with open("curso.json") as fichero:
#    data = json.load(fichero)
#    for asistentes in data['asistentes']:
#        print(f"Nombre: {asistentes['nombre']}")
#        print(f"Tecnologías utilizadas: {asistentes['conocimientos']}")
#        print(f"Tipo de asistente: {asistentes['tipo_asistente']}")
#        print(f"Aula: {asistentes['aula']}\n")


#
# Ejemplo manipular XML
#
#import xml.etree.cElementTree as ET

#print("VAMOS A GRABAR TRES MONEDAS EN UN FICHERO XML\n")

#monedas = ET.Element("monedas")

#for x in range(3):
#    print(f"Introduce la moneda número {x+1}: ", end="")
#    v_moneda = input()
#    print(f"De que país es la moneda {v_moneda}: ", end="")
#    v_pais = input()
#    print(f"Qué valor tiene la moneda {v_moneda}: ", end="")
#    v_valor = input()
#    print("")
#    nombre = ET.SubElement(monedas, "nombre", name="Nombre moneda")
#    nombre.text = v_moneda
#    pais = ET.SubElement(nombre, "pais", name="Pais moneda")
#    pais.text = v_pais
#    valor = ET.SubElement(nombre, "valor", name="Valor moneda")
#    valor.text = v_valor

#estructura = ET.ElementTree(monedas)
#estructura.write("prueba.xml")

#arbol = ET.parse("prueba.xml")
#root = arbol.getroot()

#print("Los datos del fichero XML son:")

#print(root[0].text)
#print(root[0][0].text)
#print(root[0][1].text)

#print(root[1].text)
#print(root[1][0].text)
#print(root[1][1].text)

#print(root[2].text)
#print(root[2][0].text)
#print(root[2][1].text)


