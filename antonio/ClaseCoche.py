class ClaseCoche:
    def __init__(self, marca, modelo, ano_compra, combustible, consumo_cien, precio_combustible):
        self.marca = marca                              # Marca
        self.modelo = modelo                            # Modelo
        self.ano_compra = ano_compra                    # Año de compra
        self.combustible = combustible                  # Combustible (Gasolina/Diesel)
        self.consumo_cien = consumo_cien                # Consumo en litros cada 100 kms
        self.precio_combustible = precio_combustible    # Precio actual del combustible

    def consumo_precio(self):   # Calcula el coste del combustible para 100 kms
        euros_cien = self.consumo_cien * self.precio_combustible
        return format(euros_cien, '0.2f')

    def revision_itv(self):     # Calcula los años de ITV
        anos_itv = []
        anos_itv.append(self.ano_compra+4)
        anos_itv.append(self.ano_compra+6)
        anos_itv.append(self.ano_compra+8)
        anos_itv.append(self.ano_compra+10)
        return anos_itv