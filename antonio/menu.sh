#! /bin/bash

until (($OPCION < 8)); do
	clear
	echo "******************************"
	echo "**********   MENU   **********"
	echo "******************************"
	echo
	echo "1) Usuarios logueados."
	echo "2) Logins no realizados."
	echo "3) Configuración de red."
	echo "4) Tabla de enrutamiento."
	echo "5) Servicios de red activos."
	echo "6) Mensajes del sistema."
	echo "7) SALIR"
	echo
	echo "Selecciona una opción: "
        read OPCION
done

case $OPCION in
	[1])
	echo "Los usuarios logueados son:"
	users
	   ;;

   	[2])
	echo "Los login no realizados son:"
	sudo lastb
	   ;;

	[3])
	echo "La configuración de red es:"
	ip address
	   ;;

	[4])
	echo "La tabla de enrutamiento es:"
	route -n
	   ;;
	
	[5])
	echo "Los servicios de red activos son:"
	netstat | grep "CONNECTED"
	   ;;

	[6])
	echo "Los últimos 15 mensajes del sistema son:"
	journalctl -n 15
	   ;;

	[7])
	echo
	echo "** Menú finalizado **"
	   ;;
esac
