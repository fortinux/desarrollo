#! /bin/bash
echo
echo "EXPLICACIÓN DE LOS PARÁMETROS ESPECIALES DE BASH"
echo
echo "Con la ejecución de este script que se llama $0"
echo "podemos ver los $# argumentos que se han pasado desde línea de comando."
echo "Los parámetros son $@, siendo el primero $1 y el segundo $2"
echo
echo "El identificador de este proceso (PID) es el $$"
echo
echo "Para conocer el resultado de la ejecución de un comando"
echo 'utilizamos el parámetro especial $?. Si ejecutamos el comando who'
who
echo "sabemos que la ejecuación ha sido normal por su valor $?"
echo 
echo "Para saber el último identificador de proceso ejecutado" 
echo 'en segundo plano, utilizamos el parámetro especial $!.'
echo "Ejecutamos who en segundo plano"
who &
echo "y preguntamos por su identificador $!" 
echo
