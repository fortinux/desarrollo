print("\nTE VAMOS A SOLICITAR UNA LISTA DE SEIS NÚMEROS\n")

ListaNumeros = []
NuevaLista = []

for x in range(6):
    print(f"Introduce el número {x+1} de la lista ", end="")
    Numero = int(input())
    ListaNumeros.append(Numero)

print(f"\nPerfecto. Has introducido los números {ListaNumeros}")

for x in range(6):
    if x == 0:
        NuevaLista.append(ListaNumeros[x])

    if x == 5:
        NuevaLista.append(ListaNumeros[x])

print(f"\nLa nueva lista de números con el primero y el último es {NuevaLista}")
