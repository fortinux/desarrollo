from ClaseCoche import *

obj_coche = ClaseCoche("LAMBORGHINI","Aventador",2020, "Gasolina", 12, 1.35)

print("DATOS DEL COCHE:")
print(f"  - Marca {obj_coche.marca}")
print(f"  - Modelo {obj_coche.modelo} / {obj_coche.combustible}")
print(f"  - Año de compra {obj_coche.ano_compra}")

print("\nDATOS DE CONSUMO:")
print(f"Su coche tiene un consumo de {obj_coche.consumo_cien} litros a los 100 Kms. Y el coste")
print(f"en combustible a los 100 kms es de {obj_coche.consumo_precio()} Euros")

print("\nDATOS MANTENIMIENTO:")
print(f"Años en los que tiene que pasar la ITV {obj_coche.revision_itv()}")
