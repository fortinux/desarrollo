print("\nPROGRAMA PARA COMPROBAR SI UN NÚMERO ES PAR O IMPAR\n")

Numero = int(input("Por favor, introduce un número "))

Resto = Numero % 2

if Resto == 0:
    print("El número introducido es par")
else:
    print("El número introducido es impar")

Resto = Numero % 4

if Resto == 0:
    print("y es múltiplo de 4")
else:
    print("y no es múltiplo de 4")