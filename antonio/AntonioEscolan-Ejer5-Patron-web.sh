#! /bin/bash
#
# Este ejercicio selecciona los registros que contienen "http" del fichero AntonioEscolan-Ejer4y5-Expr-Regulares.txt y los graba en fichero Direcciones-web.txt
#
grep -i "http" AntonioEscolan-Ejer4y5-Expr-Regulares.txt | cat > /home/usuario/Direcciones-web.txt
