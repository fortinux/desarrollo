from ClaseAptitudes import *

obj_empl1 = ClaseAptitudes(100, "Serena", "Williams", "68686868W", 1981, "Phyton", "Windows 10", "Ingles")
obj_empl2 = ClaseAptitudes(200, "Jeff", "Bezos", "73737373P", 1964, "Java", "Solaris", "Chino")

print("\nDATOS DEL EMPLEADO 1.-")
print(f"  Número empleado {obj_empl1.numero} {obj_empl1.nombre} {obj_empl1.apellido}")
print(f"  DNI {obj_empl1.dni}")
print(f"  Año de nacimiento {obj_empl1.ano_nacimiento}. Año de jubilación {obj_empl1.jubilacion_empleado()}")
print(f"  Lenguaje {obj_empl1.lenguaje} / Sistema Operativo {obj_empl1.so} / Idioma {obj_empl1.idioma}")
print(f"  Tienes una gratificación de {obj_empl1.gratificacion_aptitud()} Euros por conocer Linux o Python")

print("\nDATOS DEL EMPLEADO 2.-")
print(f"  Número empleado {obj_empl2.numero} {obj_empl2.nombre} {obj_empl2.apellido}")
print(f"  DNI {obj_empl2.dni}")
print(f"  Año de nacimiento {obj_empl2.ano_nacimiento}. Año de jubilación {obj_empl2.jubilacion_empleado()}")
print(f"  Lenguaje {obj_empl2.lenguaje} / Sistema Operativo {obj_empl2.so} / Idioma {obj_empl2.idioma}")
print(f"  Tienes una gratificación de {obj_empl2.gratificacion_aptitud()} Euros por conocer Linux o Python")