from ClaseEmpleados import *


class ClaseAptitudes(ClaseEmpleados):
    def __init__(self, numero, nombre, apellido, dni, ano_nacimiento, lenguaje, so, idioma):
        super().__init__(numero, nombre, apellido, dni, ano_nacimiento)
        self.lenguaje = lenguaje      # Lenguaje de programación
        self.so = so                  # Sistema Operativo
        self.idioma = idioma          # Idioma

    def gratificacion_aptitud(self):  # Gratificación por conocimiento Phyton o Linux
        if self.so == "Linux" or self.lenguaje == "Phyton":
           gratificacion = 1000
        else:
           gratificacion = 0
        return gratificacion