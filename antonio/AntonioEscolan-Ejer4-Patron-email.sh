#! /bin/bash
#
# Este ejercicio selecciona los registros que contienen "@" del fichero AntonioEscolan-Ejer4y5-Expr-Regulares.txt y los graba en fichero Direcciones-email.txt
#
grep -i "@" AntonioEscolan-Ejer4y5-Expr-Regulares.txt | cat > /home/usuario/Direcciones-email.txt
