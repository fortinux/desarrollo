#!/bin/bash
# """
# [Este script muestra ejemplos de tput]
# [Traducido y adaptado de: https://ryanstutorials.net/bash-scripting-tutorial/bash-user-interface.php ]
# [El manual oficial de tput se encuentra en https://www.gnu.org/software/termutils/manual/termutils-2.0/html_chapter/tput_1.html]
# Author: Fortinux
# Date: [2021/03/18]
# """

# Cantidad de columnas
columnas=$( tput cols )
# Cantidad de líneas
lineas=$( tput lines )
# Asigna argumentos a variable
mensaje=$@
# ${#@} dice cuantos argumentos provienen de la línea de comando
largo=${#mensaje}
# Centramos el texto
medio_largo=$(($largo / 2 ))
# Calcula como centrar el mensaje
mitad_linea=$(( $lineas / 2 ))
mitad_col=$(( ($columnas / 2) - $medio_largo ))
# tput limpia, fija el cursor al centro, aplica formato, imprime en pantalla
tput clear
tput cup $mitad_linea $mitad_col
tput bold
tput smul
echo $@
# Vuelve a la pantalla normal
tput sgr0
# Lleva el cursor al final de la pantalla
tput cup $( tput lines ) 0