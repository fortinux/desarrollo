[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/fortinux/desarrollo)

# LEEME / README #

Este README contiene la información relacionada con el repositorio del curso de GNU/Linux junto con la documentación, código fuente y snippets del mismo.

### Para qué es este repositorio? / What is this repository for? ###

* Código fuente, snippets y documentación del curso de Desarrollo que incluye:    
* LPIC 101 GNU/Linux
* LPIC 102 GNU/Linux
* Shell scripts GNU/Linux
* Python para sysadmins
    
Version 1.0    
    
### Tutorial markdown ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contacto / Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* Other things
