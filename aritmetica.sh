#! /bin/bash
# """
# [Este script presenta algunos ejemplos de operaciones matemáticas]
#
# Author: Fortinux
# Date: [2021/03/18]
# """

VAR=1
VAR=$VAR+1
echo $VAR "Variable 1+1"
RES1=$(($VAR))+1
echo $RES1 "aquí las suma"
VAR=1
set -xv

RES2=$(($VAR+1))
echo $RES2 "VAR no necesita $"
VARb=b
echo $(($VARb+1)) "texto"
set +xv

