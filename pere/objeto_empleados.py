# Importamos el módulo 'platform' para saber en qué sistema nos encontramos
import platform
# Importamos el módulo 'os' para interactuar con el sistema
import os
# Importamos las clases Empleados y Apitutdes a partir del fichero de Python clase_empleados
from clase_empleados import Empleados, Aptitudes

print(f"Nos encontramos en un sistema {platform.system()}")

# Para cada uno de los empleados que, en este caso, hay 2
for x in range(2):
    # Se crean 2 listas ya que no vienen los datos por BBDD o por fichero externo
    if x == 0:
        lista_empleado = [9999, "Juan", "99999999X", ["C", "Phyton", "Shell Script"], ["Windows", "Linux"],
                           ["castellano", "inglés", "francés"]]
    else:
        lista_empleado = [8888, "Pepe", "88888888Y", ["Phyton", "Shell Script"], ["Linux"],
                           ["castellano", "inglés"]]

    # Asignamos los datos a variables entendibles
    emple_num = lista_empleado[0]
    emple_nom = lista_empleado[1]
    emple_dni = lista_empleado[2]
    emple_lenguajes = lista_empleado[3]
    emple_so = lista_empleado[4]
    emple_idiomas = lista_empleado[5]

    # Creo el objeto empleado a partir de la clase Empleados
    empleado = Empleados(emple_num, emple_nom, emple_dni)
    # Utilizo la función imprimir de la clase Empleados
    empleado.imprimir()
    # Creo el objeto empleado_aptitudes a partir de la subclase Aptitudes hererada de Empleados
    empleado_aptitudes = Aptitudes(emple_num, emple_nom, emple_dni, emple_lenguajes, emple_so, emple_idiomas)
    # Utilizo la función imprimir_conocimientos de la subclase Aptitudes
    empleado_aptitudes.imprimir_conocimientos()

    if x == 0:
        print(input("* **************** Teclea Enter para el siguiente registro"))
        # Pregunto en qué sistema me encuentro para limpiar pantalla entre empleados
        if platform.system() == 'Windows':
            os.system("cls")
        else:
            os.system("clear")