from pathlib import Path
import os

# # Se obtiene el directorio inicio del usuario
# fichero_path = Path(Path.home(), "Python_ejemplos_rutasficheros")
# print(fichero_path)
#
# # Si no existe el directorio, se agrega
# if not fichero_path.exists():
#     os.makedirs(fichero_path)
#
# # Se agrego el fichero al path
# fichero_path = fichero_path.joinpath("quijote-ext03.txt")
#
# # se escriben las siguientes líneas en el fichero
# with fichero_path.open('w', encoding='utf-8') as file:
#     lineas = [
#         "Primera parte del ingenioso hidalgo don Quijote de la Mancha \n"
#         "Capítulo primero. Que trata de la condición y ejercicio del famoso \n"
#         "hidalgo don Quijote \n"
#     ]
#     file.writelines(lineas)
#
# Otros ejemplos leyendo de un directorio en la actual ruta, y de un directorio superior:
# Lee del directorio "directorio1"
fichero_path = Path("fin", "quijote-ext02.txt")
print(fichero_path)
with fichero_path.open('r', encoding='utf-8') as file:
    print(file.read())
file.close()

# Lee del directorio superior
fichero_path = Path(os.pardir, "docs", "cheat_sheet_pycharm.md")
print(fichero_path)
print(os.path)
with fichero_path.open('r', encoding='utf-8') as file:
    print(file.read())
file.close()
