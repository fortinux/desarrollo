import Pyro4
import example
from f_pyro_persona import Person

uri = input("Enter the uri of the warehouse: ").strip()  #PYRO:example.warehouse@localhost:49521
warehouse = Pyro4.Proxy(uri)
janet = Person("Janet")
henry = Person("Henry")
janet.visit(warehouse)
henry.visit(warehouse)