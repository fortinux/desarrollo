import json

ruta_in = 'fin\\'
ruta_out = 'fout\\'

# Ejemplo de string en JSON
persona = '{"nombre": "Fortinux", "lenguajes": ["Python", "PHP"]}'
print('1-', persona)
# Parse usando el método json.loads()
# Crea un diccionario en python
persona_dic = json.loads(persona)
print('2.1-', persona_dic['lenguajes'])
print('2.2-', persona_dic)

# Convierte diccionario a JSON
# .dumps (encode) toma un diccionario como input y devuelve un string
persona_json = json.dumps(persona_dic)
print('3-', persona_json)

# Leer un fichero JSON
with open(ruta_in + 'fichero_ejemplo.json') as fichero:
    # .loads (decode) toma un string como input y devuelve un diccionario
    datos = json.loads(fichero.read())
print('4-', datos)
# Lee solo un dato del fichero
print('5-', datos["Nombre"])
#
# Exporta “datos” a un fichero .json
with open(ruta_out + 'ejemplo_persona_JSON.txt', 'w') as json_fichero:
    json.dump(datos, json_fichero)
