import mechanize


def page_view(url):
    try:
        # crea el objeto navegador
        browser = mechanize.Browser()
        # browser.set_handle_robots(False)
        page = browser.open(url)
        src_code = page.read()
        # Imprime el código fuente de la página web
        print(src_code)
    except:
        print("Error al navegar...")


# url = "https://fortinux.com"
url = "https://www.amazon.es"
page_view(url)

