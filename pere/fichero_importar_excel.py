import pandas as pd
# import xlrd
# import openpyxl

ruta_in = 'fin\\'
ruta_out = 'fout\\'

# Se crea el dataframe con pandas de un fichero Excel
# dataframe_excel = pd.read_excel(ruta_in + "catalogo_cf.xlsx")
# print(dataframe_excel)
# # se crea el dataframe con pandas de un fichero .csv
# dataframe_csv = pd.read_csv(ruta_in + "catalogo_cf.csv")
# print(dataframe_csv.iloc[:, 3])
# # Selecciona la primera y la cuarta columna
dataframe_csv = pd.read_csv(ruta_in + "catalogo_cf.csv")
print(dataframe_csv.iloc[:, [0, 3]])
# # se exporta a excel
dataframe_csv.iloc[:, 0-10].to_excel(ruta_out + 'catalogo_cf_copia2.xlsx')
