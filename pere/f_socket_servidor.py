# Fichero socket_servidor.py
import socket

# Crea el objeto socket. Protocolo AF_INET (TCP/IP) + tipo comunicación SOCK_STREAM
serversocket = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)

# Obtiene el nombre de la máquina local y establece un puerto
host = socket.gethostname()
port = 20000

# Conecta el host al puerto
serversocket.bind((host, port))

# Lista hasta 10 solicitudes
serversocket.listen(10)

while True:
    # establece la conexión
    clientsocket, addr = serversocket.accept()
    print("Se ha obtenido una conexión de %s" % str(addr))
    msg = 'Muchas gracias' + "\r\n"
    clientsocket.send(msg.encode('ascii'))
    clientsocket.close()
