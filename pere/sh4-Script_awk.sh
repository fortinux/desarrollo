#!/bin/bash
 
continuar(){
  read -p "Presiona [Enter] para continuar..." fackEnterKey
}
 

leer_optiones(){
 local FICHERO=/etc/passwd
 read -p "Elige una de las opciones del MENU: " ELECCION
 case $ELECCION in
  1) cat $FICHERO | awk 'BEGIN {FS=":"} ; {print $1,$3,$4}'
     continuar 
  ;;
  2) cat $FICHERO | awk 'BEGIN {FS=":"} ; {print NR " - " $1 " - " $3 " - " $4} ; END {print "TOTAL LINEAS NR="NR}'
     continuar
  ;;
  3) cat $FICHERO | awk 'BEGIN {FS=":"} ; {print $0=++numlin " - " $1 " - " $3 " - " $4} ; END {print "TOTAL LINEAS contador="numlin}'
     continuar
  ;;
  4) exit 0;;
  *) echo "Error!!! vuelve a intentarlo" && sleep 2
 esac
}
 
# Men� repetitivo
while true
do
 clear
 echo "	***********************" 
 echo "	*                     *"
 echo "	*   Menu-Script awk   *"
 echo "	*                     *"
 echo -e "	***********************\n" 
 echo "	 1) Muestre las columnas del nombre de usuario, ID de usuario e ID de grupo."
 echo "	 2) Cuente las lineas del resultado con NR."
 echo "	 3) Cuente las lineas del resultado con un contador."
 echo -e "	 4) SALIR\n"

 leer_optiones
 
done
