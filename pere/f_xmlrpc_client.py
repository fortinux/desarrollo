# Fichero CLIENTE XMLRPC
import xmlrpc.client
import fichero_xmlrpc_server

# Se conecta por proxy al servidor en el puerto 10000
proxy = xmlrpc.client.ServerProxy('http://localhost:10000')
# Lista el contenido el directorio /tmp
print(proxy.lista_contenidos('/tmp'))
