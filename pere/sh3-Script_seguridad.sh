#!/bin/bash
 
continuar(){
  read "Presiona [Enter] para continuar..." fackEnterKey
}
 

leer_optiones(){
 read  "Elige una de las opciones del MENU: " ELECCION
 case $ELECCION in
  1) who | cut -d " " -f1
     continuar 
  ;;
  2) sudo lastb | less ;;
  3) ifconfig
     continuar 
  ;;
  4) netstat -r 
     continuar 
  ;;
  5) netstat -v | less ;;
  6) dmesg | tail -n15
     continuar 
  ;;
  7) exit 0;;
  *) echo "Error!!! vuelve a intentarlo" && sleep 2
 esac
}
 
# Men� repetitivo
while true
do
 clear
 echo "	*****************************" 
 echo "	*                           *"
 echo "	*   Menu-Script seguridad   *"
 echo "	*                           *"
 echo -e "	*****************************\n" 
 echo "	 1) Usuarios logueados"
 echo "	 2) Logins no realizados"
 echo "	 3) Configuracion de red"
 echo "	 4) Tabla de enrutamiento"
 echo "	 5) Servicios de red activos"
 echo "	 6) Mensajes del sistema"
 echo "	    (ultimos 15 mensajes)"
 echo -e "	 7) SALIR\n"

 leer_optiones
 
done
