#!/bin/bash

echo "Con el PID |$$| --> "'$$'
echo "estamos ejecutando el script |$0| --> "'$0'
echo "que tiene |$#| parámetros --> "'$#'
echo "que son |$@| --> " '$@'
echo " - el primer parámetro es |$1| --> "'$1'
echo " - el segundo parámetro es |$2| --> "'$2'
echo "El código de retorno de la ejecución del 2º parámetro es |$?| --> "'$?'
ls /ERROR/
echo "y el código de retorno del error es |$?| --> "'$?'
