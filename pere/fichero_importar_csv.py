import pandas as pd

ruta_in = 'fin\\'
ruta_out = 'fout\\'

# Variables con los ficheros a importar
fichero_csv = ruta_in + "catalogo_cf.csv"
fichero_tsv = ruta_in + "catalogo_cf.tsv"

# Nombres de los ficheros a escribir
escribir_csv = ruta_out + 'catalogo_csv_ext.csv'
escribir_tsv = ruta_out + 'catalogo_tsv_ext.tsv'

# Lee los datos de los ficheros
leer_csv = pd.read_csv(fichero_csv)
leer_tsv = pd.read_csv(fichero_tsv, sep='\t')

# Imprime los primeros 10 registros
print(leer_csv.head(10))
print(leer_tsv.head(10))

# Escribe a los ficheros: en el .csv solamente los 10 primeros registros
with open(escribir_csv, 'w') as write_csv:
    write_csv.write(leer_csv.head(10).to_csv(sep=',', index=False))

with open(escribir_tsv, 'w', encoding='utf-8') as write_tsv:
    write_tsv.write(leer_tsv.to_csv(sep='\t', index=False))
