import xml.etree.ElementTree as ET

ruta_in = 'fin\\'
ruta_out = 'fout\\'

# Importar XML desde un fichero
tree = ET.parse(ruta_in + 'ejemplo_country_data.xml')
root = tree.getroot()
# Acceder a elementos determinados
for child in root:
    print(child.tag, child.attrib)
# Acceder al child utilizando índices
print(root[0][1].text)

# Acceder a sub elemento utilizando el método Element
for neighbor in root.iter('neighbor'):
    print(neighbor.attrib)

# Modificar un fichero XML y escribirlo a un nuevo fichero
for rank in root.iter('rank'):
    new_rank = int(rank.text) + 5
    rank.text = str(new_rank)
    rank.set('updated', 'yes')
tree.write(ruta_out + 'ejemplo_country_data_modificado.xml')