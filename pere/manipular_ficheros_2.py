# Biblioteca os para interactual con el sistema
import os
# Biblioteca gestión de Paths
from pathlib import Path
# Biblioteca para fechas
from datetime import datetime, time
# Biblioteca para identificar o comparar con otro fichero
import hashlib
# Biblioteca para tratamiento de ficheros json
import json
# Biblioteca para tratamiento de ficheros XML
import xml.etree.ElementTree as ET

dirIn = "fin"
dirOut = "fout"
file_TXT1 = "quijote_pg2000.txt"
file_TXT2 = "quijote-ext03.txt"
file_TXT3 = "quijote-ext04.txt"
file_json = "fichero_ejemplo2.json"
file_output_json = "fichero_ejemplo2_output.json"
file_xml = "ejemplo_country_data.xml"
file_output_xml = "ejemplo_country_data_output.xml"
curdir = os.getcwd()
os.system("cls")

input("1.- Ejemplo Comparar ficheros")
for linea in open(Path(dirIn, file_TXT2), encoding='utf-8'):
    if linea not in open(Path(dirIn, file_TXT3), encoding='utf-8'):
        print(linea)

input("Teclea ENTER para continuar")
os.system("cls")


input("2.- Ejemplo crear hash")

input("Compara sin hash")
tiempoIni1 = datetime.now().strftime("%M:%S")  # Extraigo la hora del momento al iniciar el proceso1
tiempoIni1 = datetime.strptime(tiempoIni1, "%M:%S")  # doy el formato a esta hora

for linea in open(Path(dirIn, file_TXT1), 'r', encoding='utf-8'):  # el mismo código del punto 1 para comparar tiempos
    if linea not in open(Path(dirIn, file_TXT2), 'r', encoding='utf-8'):
        print(linea)
tiempoFin1 = datetime.now().strftime("%M:%S")   # Extraigo la hora del momento al finalizar el proceso1
tiempoFin1 = datetime.strptime(tiempoFin1, "%M:%S")
resta1 = tiempoFin1-tiempoIni1
print("Tiempo de proceso sin hash --> " + str(resta1))  # tiempo total de proceso 1

input("Compara con hash")
tiempoIni2 = datetime.now().strftime("%M:%S")    # Extraigo la hora del momento al iniciar el proceso2
tiempoIni2 = datetime.strptime(tiempoIni2, "%M:%S")

lineasTXT2 = {hash(linea) for linea in open(Path(dirIn, file_TXT2), 'r', encoding='utf-8')}  # leo todas las líneas
for linea in open(Path(dirIn, file_TXT1), 'r', encoding='utf-8'):                             # utilizando hash
    if hash(linea) not in lineasTXT2:  # Si el hash de la línea del TXT1 son se encuentra en el hush del TX2
        print(linea)                    # imprime la línea

tiempoFin2 = datetime.now().strftime("%M:%S")  # Extraigo la hora del momento al finalizar el proceso2
tiempoFin2 = datetime.strptime(tiempoFin2, "%M:%S")
resta2 = tiempoFin2-tiempoIni2
print("Tiempo de proceso con hash --> " + str(resta2))  # tiempo total de proceso 2
print(str(resta1-resta2), 'segundos de diferencia con hash.')  # Diferencia de tiempo del proceso 1 y 2

input("Teclea ENTER para continuar")
os.system("cls")

input("3.- Ejemplo manipular JSON")
with open(Path(dirIn, file_json), encoding='utf-8') as fichero:
    datos = json.load(fichero)          # creo un diccionario e imprimo los índices de éstos
    for cliente in datos['Clientes']:
        print('Nombre:', cliente['Nombre'])
        print('Apellidos:', cliente['Apellidos'])
        print('Género:', cliente['Genero'])
        print('Dirección:')
        print('\tCalle:', cliente['Direccion']['Calle'])
        print('\tCiudad:', cliente['Direccion']['Ciudad'])
        print('\tCódigo Postal:', cliente['Direccion']['Codigo Postal'])
        print('Teléfonos:')
        print('\tTipo/número:', cliente['Telefonos']['tipo'] + '/' + cliente['Telefonos']['numero'])
        print('')
print(datos)

with open(Path(dirOut, file_output_json), 'w', encoding='utf-8') as fichero:
    json.dump(datos, fichero, indent=4)     # imprimo un nuevo fichero json el diccionario indentádolo con 4 caracteres

input("Teclea ENTER para continuar")
os.system("cls")


input("4.- Ejemplo manipular XML")
tree = ET.parse(Path(dirIn, file_xml))
print(tree)
root = tree.getroot()
print(root)
for nodo in root:
    if nodo.attrib.get('name'):
        print('nodo:', nodo.tag, '- Atributo:', nodo.attrib.get('name'))
    else:
        print('nodo:', nodo.tag)
    for subnodo in nodo:
        if subnodo.attrib.get('name'):
            print('\tsubnodo:', subnodo.tag, '- Atributos:', subnodo.attrib.get('name'), subnodo.attrib.get('direction'))
        else:
            print('\tsubnodo:', subnodo.tag, '- Valor:', subnodo.text)
