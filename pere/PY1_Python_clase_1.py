import time
import os

os.system("cls")
print('Ejercicio 1: Crear un programa que le pregunte al usuario su '
      'nombre y edad y le muestre el año en que cumplirá los 100 años.')

nombre = input("Dime tu nombre: ")
edad = int(input("Dime la edad que tendrás al terminar este año: "))

print("Hola " + nombre + " cumplirás los 100 años en el año " + str(int(time.strftime("%Y")) - edad + 100))

input("Teclea ENTER para continuar")
os.system("cls")

print('Ejercicio 2: Preguntar al usuario un número y mostrar si es par o impar. '
      'Si el número es múltiple de 4 imprimir el mensaje apropiado al usuario.')

numero = int(input("Entra un número: "))
if numero % 2 == 0:
    print(f"El número {str(numero)} es par")
else:
    print(f"El número {str(numero)} es impar")

if numero % 4 == 0:
    print(f"El número {str(numero)} es múltiplo de 4")
else:
    print(f"El número {str(numero)} no es múltiplo de 4")

input("Teclea ENTER para continuar")
os.system("cls")

print('Ejercicio 3: Escribir un programa que tome una lista de números '
      '(entre 5 y 10 números) y haga una lista con el primero y el último de los elementos.')

lista_numeros1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
lista_numeros2 = list(range(1, 11))
print('     Los elementos de la lista1 son ' + str(lista_numeros1))
print('     Los elementos de la lista2 son ' + str(lista_numeros2))
print('     El primer elemento de la lista1 es ' + str(lista_numeros1[0]))
print('     El último elemento de la lista2 es ' + str(len(lista_numeros2)))
