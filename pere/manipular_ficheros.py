# Biblioteca os para interactual con el sistema
import os
# Biblioteca shutil para manipulaci�n de ficheros
import shutil
# Biblioteca gestión de Paths
from pathlib import Path
# Biblioteca para ficheros CVS y TSV
import pandas as pd
# Biblioteca para comparar ficheros
import filecmp

dirIn = "fin"
dirOut = "fout"
dirCopy = "fcopy"
file_CSV = "01001.csv"
file_TSV = "01001.tsv"
file_Copia = "01001.copia.csv"
file_XLSX = "01001.xlsx"
file_CSV_TSV = Path("fout","01001.CSVaTSV.tsv")
file_100_TSV = Path("fout","01001.100.tsv")
file_5cols_XLSX = Path("fout","01001.5colums.xlsx")
curdir = os.getcwd()

os.system("cls")

input("1.- Crear una copia y luego convertir el fichero .csv a .tsv")
os.system("dir /B " + str(Path(dirIn, "*.csv")))

print(f"* **************Ahora vamos a copiar el fichero {file_CSV} como {file_Copia}")
shutil.copy(src=str(Path(dirIn, file_CSV)), dst=str(Path(dirIn, file_Copia)))
os.system("dir /B " + str(Path(dirIn, "*.csv")))

# Ahora convertiremos el fichero
leer_csv = pd.read_csv(Path(dirIn, file_CSV), sep=';')
with open(file_CSV_TSV, 'w', encoding='utf-8', newline='') as write_tsv:
    write_tsv.write(leer_csv.to_csv(sep='\t', index=False))
write_tsv.close()

print(f"* **************Ahora vamos a visualizar el fichero {file_CSV_TSV}")
os.system("dir /B " + str(Path(dirOut, "*.tsv")))

input("Teclea ENTER para continuar")
os.system("cls")


input("2.- Crear un directorio y mover la copia del fichero .csv dentro")
os.system("dir /AD " + str(Path(dirIn)))
if not Path(dirIn, dirCopy).exists():
    print(f"* **************Ahora vamos a crear el directorio {dirCopy}")
    os.makedirs(Path(dirIn, dirCopy))

if Path(dirIn, file_Copia).exists():
    shutil.move(src=str(Path(dirIn, file_Copia)), dst=str(Path(dirIn, dirCopy, file_Copia)))
os.system("dir " + str(Path(dirIn, dirCopy)))

input("Teclea ENTER para continuar")
os.system("cls")


input("3.- Escribir en un nuevo fichero las primeras 100 líneas del fichero .tsv")
leer_tsv = pd.read_csv(str(Path(dirIn, file_TSV)), sep='\t')
with open(file_100_TSV, 'w', encoding='utf-8', newline='') as write_tsv:
    write_tsv.write(leer_tsv.head(99).to_csv(sep='\t', index=False))
os.system("dir /B " + str(Path(dirOut, "*.tsv")))

input("Teclea ENTER para continuar")
os.system("cls")


input("4.- Comparar los datos dentro de los ficheros .csv")

son_iguales = str(filecmp.cmp(Path(curdir, dirIn, file_CSV), Path(curdir, dirIn, dirCopy, file_Copia)))
son_iguales = son_iguales.replace("True", "son iguales")
son_iguales = son_iguales.replace("False", "No son iguales")
print(f"¿son iguales los ficheros {file_CSV} y {file_Copia}? -->  {son_iguales}\n")

input("Teclea ENTER para continuar")
os.system("cls")


input("5.- Exportar las primeras 5 columnas del fichero en formato .xlsx a un nuevo fichero.")

datos_excel = pd.read_excel(Path(dirIn, file_XLSX))
datos_excel.iloc[:, 0:5].to_excel(file_5cols_XLSX, index=False)
