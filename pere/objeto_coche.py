from clase_coche import Coche

# Creando las instancias de la clase Coche
objeto_coche = Coche('SEAT', 'Ateca', '1.0')

# Acceder a los atributos de ese objeto
print(objeto_coche.marca)
print(objeto_coche.modelo)
print(objeto_coche.tipo)

# Llamando a los métodos de la clase
print()
objeto_coche.gasolina_completo()
objeto_coche.conducir()