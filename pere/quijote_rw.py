# Lee el fichero
# fichero = open("fin/quijote_pg2000.txt", 'r', encoding='utf-8')
# for linea in fichero:
#     print(linea)
# fichero.close()

# Lee los primeros 200 caracteres de un fichero
# with open("fin/quijote_pg2000.txt", 'r', encoding='utf-8') as fichero:
#     contenido = fichero.read(200)
#     print(contenido)

# Lee la primera línea de un fichero
# with open("fin/quijote_pg2000.txt", 'r', encoding='utf-8') as file:
#     contenido = file.readline()
#     print(contenido)

# # Lee los párrafos de un fichero.
# # Strip() evita una línea en blanco entre ellos.
# with open("fin/quijote_pg2000.txt", 'r', encoding='utf-8') as file:
#     contenidos = file.readlines(2000)
#     for c in contenidos:
#         print(c.strip())
#     # contenidos = file.read().strip()
#     # print(contenidos)
#
# file.close()



entrada = """Primera parte del ingenioso hidalgo don Quijote de la Mancha


"""
# Creamos un fichero y pegamos el texto de la variable entrada
with open("fout/texto_escribir.txt", 'x', encoding='utf-8') as file:
    file.write(entrada)
file.close()

entrada_agregar = """En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha
mucho tiempo que vivía un hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo
corredor. Una olla de algo más vaca que carnero, ...


"""
# Abrimos el fichero y adjuntamos el texto de la variable "entrada_agregar
with open("fout/texto_escribir.txt", 'a', encoding='utf-8') as file:
    file.write(entrada_agregar)
file.close()
