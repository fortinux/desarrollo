# Fichero socket_cliente.py
import socket

# Crea el objeto socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Obtiene el nombre de la máquina local
host = socket.gethostname()
port = 20000
# Conecta el host al puerto
s.connect((host, port))
# Recibe hasta 1024 bytes
msg = s.recv(1024)
s.close()
print (msg.decode('ascii'))
