# Se crea la clase Empleados
class Empleados:
    def __init__(self, numero, nombre, dni):
        self.numero = numero
        self.nombre = nombre
        self.dni = dni

    def imprimir(self):
        print(f'{self.nombre} con DNI {self.dni} tiene el número de empleado {self.numero}.')


# Se crea la subclase Aptitudes hererando de la clase Empleados
class Aptitudes(Empleados):
    def __init__(self, numero, nombre, dni, lenguajes, so, idioma):
        super().__init__(numero, nombre, dni)
        self.lenguajes = lenguajes
        self.so = so
        self.idioma = idioma

    def imprimir_conocimientos(self):
        print(f"Los conocimientos de {self.nombre} son:")
        print(f" - Lenguajes de programación:")
        for x in self.lenguajes:
            print(f"\t· {x}")
        print("\n - Sistemas operativos:")
        for x in self.so:
            print(f"\t· {x}")
        print("\n - Idiomas:")
        for x in self.idioma:
            print(f"\t· {x}")