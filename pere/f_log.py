import logging

ruta_in = 'fin\\'
ruta_out = 'fout\\'

# Envía la información del log a un fichero
logging.basicConfig(filename=ruta_in + 'ejemplo_log.log.txt', level=logging.ERROR)

# Niveles de error
print(logging.debug("Debugging"))
print(logging.info("Información"))
# Para reproducir el error
logging.warning("Esta es una advertencia!")
logging.error("Error")
logging.critical("Error crítico")